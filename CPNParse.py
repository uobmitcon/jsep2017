#!/usr/bin/env python
# Phil Weber 2016-11-18

""" CPNParse: Class for processing variable definitions, transition guards and
              arc inscriptions in Coloured Petri Net definitions.

Summary:
  Limited version of CPN/Tools functionality.
  1.  Variable definitions but no colour sets.
        vardef :: "var" var (, var)* : type ;
        var    :: [A-Za-z][A-Za-z0-9]*
        type   :: "BOOL" | "INT" | "REAL" | "STRING"
  2.  Transition guards.
        guard :: ['['] [not | !] var [op (real | int)] [']']
        op    :: = | >[=] | <[=]
        int   :: [0-9]+
        real  :: [0-9]+[.[0-9]*]
        Should allow:   x
                      ! x # ! not
                        x > 1 # > < >= <=
                        x =[=] {false|true}
  3.  (Post-transition) variable modification.
        mod :: [(] mod1+[)]
        mod1 :: var = [var op] num
        mod1 :: var = [var +] string
        mod1 :: [neg] var=bool
        op  :: + | - | * | /
        Should allow: x =   x + 1
                            x + 1 # implicitly modify x (CPN/Tools)
                      x = ! x
                      x =       3 # assignment (CPN/Tools doesn't do this)
                            x     # unchanged

To do:
  o  Formalise as a very limited Domain Specific Language (DSL).

Limitations:
  o  Only implimented explicit options; behaviour outside of these may be
     unpredictable (i.e. limited testing on the DSL): DSL and behaviour need
     to be completely specified.
  o  Some concerns over the scope of variables.
  o  Not allowed string comparisons on guards.
  o  Creation of test values for guards, in guard_testvals, is quite naive.

Changes:
  o  2016-11-22 Allow arc inscriptions both setting a variable (e.g. "a=a+2"),
     or as CPN/Tools, e.g. "a+2", "(a + 2)", "(b,c,a-1,d)" (as per CPN/Tools,
     (not extended code, conditional inscriptions).
  o  2016-11-28 Allow transition guards to have CSV-separated list of guards,
     e.g. CS<=1,NSAID<=1.
  o  2016-11-30 Enabled guards such as "b == False".
# o  2016-12-06 Changed updval to ignore missing variables, on the assumption
#    that these are inscription variables which feature on no guards and thus
#    on no tokens.
"""

from __future__ import print_function
from sys import path

try:
  from pyparsing import *
except:
  # CS location of pyparsing
  path.append("/usr/lib64/python2.6/site-packages/matplotlib")
  from pyparsing import *


class parser(object):
  """ Top-level object containing grammar objects for interpreting
      - variable definitions
      - variable assignments
      - variable checks (transition guards)
      - variable modifications (arc inscriptions)
  """
  sbool = CaselessLiteral("BOOL")
  sint = CaselessLiteral("INT")
  sreal = CaselessLiteral("REAL")
  sstring = CaselessLiteral("STRING")
  svar = Suppress(Literal("var"))

  comma = Suppress(Literal(','))
  colon = Suppress(Literal(':'))
  q = Suppress(Optional(Literal("'") | Literal('"')))

  var = Word(alphas + nums + '_').setResultsName("var")
  false = CaselessLiteral("False")
  true = CaselessLiteral("True")
  vbool = (false | true)
  vint = Word(nums)
  vreal = Combine(Word(nums) + '.' + Optional(Word(nums)))
  vstring = q + Word(alphas + '_' + '-' + ' ') + q

  lsq = Suppress(Optional('['))
  rsq = Suppress(Optional(']'))
  lb = Suppress(Optional('('))
  rb = Suppress(Optional(')'))
  eq = (Literal('==') | Literal("=")).setParseAction(replaceWith("==")) \
         .setResultsName("eq")
  gt = Literal('>')
  lt = Literal('<')
  ge = Literal(">=")
  le = Literal("<=")
  op = (ge|le|eq|gt|lt).setResultsName("op") # NB order important.

  pl = Literal('+')
  mi = Literal('-')
  mu = Literal('*')
  di = Literal('/')

  genval = (vreal | vint | vbool | vstring).setResultsName("genval")
  numval = (vreal | vint).setResultsName("numval")
  numboolval = (vreal | vint | vbool).setResultsName("numboolval")

  neg = (Literal("not") | Literal('!')).setParseAction(replaceWith("not")) \
        .setResultsName("neg")


  class vardefGrammar(object):
    def __init__(self):
      """ Grammar for variable definitions. """
      self.vtype = (parser.sbool|parser.sint|parser.sreal|parser.sstring) \
                   .setResultsName("vtype")
      self.vardef = parser.svar + parser.var + ZeroOrMore(parser.comma \
                  + parser.var) + parser.colon + self.vtype

    def parse(self, exp):
      results = self.vardef.parseString(exp)
      return (results[0:-1], results.vtype)


  class varassGrammar(object):
    def __init__(self):
      """ Grammar for variable assignments (test only). """
      self.ass = parser.var + '=' + parser.genval

    def parse(self, exp):
      results = self.ass.parseString(exp)
      return (results.var, results.genval)

    def assign(self, vdict, var, val):
      try:
        t = vdict[var]["type"]
      except:
        raise Exception("Variable", var, "not in dict")
      # Check type: perhaps the parsing should have already done this?
      try:
        if t == "INT":
          vdict[var]["value"] = int(val)
        elif t == "REAL":
          vdict[var]["value"] = float(val)
        elif t == "BOOL":
          vdict[var]["value"] = (val.lower() == "true")
        elif t == "STRING":
          vdict[var]["value"] = val
        else:
          raise Exception("Variable", var, "has undefined type", t, "in dict")
      except:
        raise Exception("Assignment", val, "to var", var, "failed type check")
      return vdict


  class tguardGrammar(object):
    def __init__(self):
      """ Grammar for transition guards. """
      self.guard = parser.lsq + Optional(parser.neg) + parser.var \
                   + Optional(parser.op + parser.numboolval) + parser.rsq

    def parse(self, exp, vdict, checkNone=False):
      results = self.guard.parseString(exp)

      var,op,val,neg = results.var, results.op, results.numboolval, results.neg
      try:
        t = vdict[var]["type"]
      except:
        raise Exception("Variable", var, "not in dict")
      if checkNone and vdict[var]["value"] is None:
        raise Exception("Variable", var, "unset (None) in dict")
      try:
        if t in ["INT", "REAL"]:
          newval = float(val)
        elif t == "BOOL":
          newval = (val.lower() == "true")
        elif t == "STRING":
          raise Exception("Variable", var, "has disallowed type", t, "in dict")
        else:
          raise Exception("Variable", var, "has undefined type", t, "in dict")
      except:
        raise Exception("Assignment", val, "to var",
                        var, "type", t, "failed type check")

      return (var, op, val, neg)

    def guard_testvals(self, exp, vdict):
      """ Parse guard and return a list of (var name, (pair of values)) tuples
          for testing. """
      returnlist = []
      if exp not in [None, ""]:
        guards = exp.split(',')
        for exp in guards:
          results = self.parse(exp, vdict, checkNone=False)
          if results is not None:
            var, op, val, neg = results
            neg = None if neg in [None, ''] else neg
            var = None if var in [None, ''] else var
            op = None if op in [None, ''] else op
            val = None if val in [None, ''] else val
            if op is val is None \
            or op == "==" and val in ["True", "False"]:
              # Boolean 'x' or "not x": check true & false cases
              if vdict[var]["type"] == "BOOL":
                checkvals = (True, False)
              else:
                raise Exception("Guard boolean test for non-boolean var", var)
              
            elif neg is None \
                and var is not None and op is not None and val is not None:
              # Bad: enforcing parsing after the event.
              # >, >=, <, <=: check (close) boundary cases
              if vdict[var]["type"] in ["INT", "REAL"]:
                val = float(val)
                if op in [">=", '<']:
                  checkvals = (val-1, val)
                elif op in ['>', "<=", '=', "=="]:
                  checkvals = (val, val+1)
                else:
                  raise Exception("Prob. with guard cond. for token creation.")
            else:
              raise Exception("Problem parsing guard for token creation.")
            returnlist.append((var, checkvals))
      return returnlist


  class arcinsGrammar(object):
    def __init__(self):
      """ Grammar for (post-transition) variable modification (on arc). """
      self.modop = (parser.pl|parser.mi|parser.mu|parser.di) \
                   .setResultsName("modop")
      self.modval = (parser.vstring|parser.numval).setResultsName("modval")
      self.mod = \
        Optional(parser.var.setResultsName("assvar") \
               + '=' \
                ) \
      + Optional(parser.neg.setResultsName("modneg")) \
      + Optional(parser.var.setResultsName("modvar") \
               + self.modop.setResultsName("modop") \
                ) \
      + self.modval.setResultsName("modval")

    def parse(self, exp):
      """ Non-pyparsing removal of leading ( and trailing ), and split into CSV.
      """
      if len(exp) > 0 and exp[0] == '(':
        exp = exp[1:]
      if len(exp) > 0 and exp[-1] == ')':
        exp = exp[:-1]
      results = exp.split(',')
      return results

    def parse1(self, vdict, mod, checkNone=False):
      """ Parse a single mod as split by parse().
          NB: Checks variable in dictionary but no type checking.
      """
      results = self.mod.parseString(mod)

      modneg = None if results.modneg in [None, ""] else results.modneg
      modvar = None if results.modvar in [None, ""] else results.modvar
      assvar = None if results.assvar in [None, ""] else results.assvar
      modop = None if results.modop in [None, ""] else results.modop
      modval = None if results.modval in [None, ""] else results.modval

      # This is a bodge; grammar should be corrected.
      if modvar is None and assvar is not None:
        modvar = assvar
      try:
        t = vdict[modvar]["type"]
      except:
        raise Exception("Variable", modvar, "not in dict")

      return (modneg, modvar, assvar, modop, modval)

    def updval(self, vdict, modlist):
      """ Update based in parsing each in a list of CSV modification stmts. """
      for exp in modlist:
        results = self.mod.parseString(exp)

        modneg = None if results.modneg in [None, ""] else results.modneg
        modvar = None if results.modvar in [None, ""] else results.modvar
        assvar = None if results.assvar in [None, ""] else results.assvar
        modop = None if results.modop in [None, ""] else results.modop
        modval = None if results.modval in [None, ""] else results.modval

        # Bodge: if single var, it should be modvar (e.g. 'x' : unchanged).
        # Unsure how to create the grammar to set this as default.
        if assvar is modneg is modvar is modop is None and modval is not None: 
          modvar, modval = modval, None

        if assvar is None:
          assvar = modvar

        # Ignore statements of the type "x = x" or sole 'x' (CPN/Tools).
        if not ((assvar == modvar) and (modneg is modop is modval is None)):
          newval = None
          if modval is not None:
            try:
              t = vdict[assvar]["type"]
            except:
              # Silently ignore assuming this means that the variable is not
              # part of the tokenset, i.e. not on any guard.
              return vdict
              # raise Exception("Variable", assvar, "not in dict")
            if vdict[assvar]["value"] is None:
              raise Exception("Variable", assvar, "unset (None) in dict")
            try:
              if t == "INT":
                newval = int(modval)
              elif t == "REAL":
                newval = float(modval)
              elif t == "BOOL":
                newval = (modval.lower() == "true")
              elif t == "STRING":
                newval = '"' + modval + '"'
              else:
                raise Exception("Variable", assvar, "has undefined type", t,
                                "in dict")
            except:
              raise Exception("Assignment", modval, "to var",
                              assvar, "type", t, "failed type check")

          S = "vdict['" + assvar + "']['value'] = "
          if modneg not in [None, ""]:
            S += "not "
          if modvar not in [None, ""]:
            S += "vdict['" + modvar + "']['value'] "
          if modop not in [None, ""]:
            S += modop + ' '
          if newval not in [None, ""]:
            S += str(newval)
          exec(S)

      return vdict

  def __init__(self):
    """ Define grammar. """
    self.vardef = self.vardefGrammar()
    self.varass = self.varassGrammar()
    self.tguard = self.tguardGrammar()
    self.arcins = self.arcinsGrammar()

