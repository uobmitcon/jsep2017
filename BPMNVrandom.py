#!/usr/bin/env python2
# Phil Weber 2016-12-07 Experimental framework for BPMNV + CPN Analsyis with
# composition.

# LIMITATIONS: counting of conflicts, comparing generated with discovered.

from __future__ import print_function
import sys
from os import path, devnull, system
import cPickle as pickle
import numpy as np
from CPN import *
from BPMNV import *
import CPNParse
from CPNAnalysis import CPNAnalyser

sep = ','  # for output


if __name__ == "__main__":
  """ Parameters: 1: Size of model (#Activities) to go up to.
      (Draft)     2: Number of each size model to test (stats).
                  3: Number of conflicts to put in.
  """
  bPrint = False
  log = False
  timings = False
  output_bpmn = False
  output_cpn = False
  output_rg = False
  minmodelsize = 2  # No point testing 1-activity models

  from optparse import OptionParser
  op = OptionParser()
  op.add_option('-s', '--modelsize', dest="maxmodelsize",
                help="Number of activities in final model",
                type="int", default=1)
  op.add_option('-n', '--models', dest="models",
                help="Number samples of each size model",
                type="int", default=1)
  op.add_option('-c', '--conflicts', dest="conflicts",
                help="Number guard/data conflicts between each model pair",
                type="int", default=1)
  op.add_option('-p', '--params', dest="params",
                help="Opt. [p_seq,p_xor,p_ior,p_and,p1m1v,p1m2v,p2m1v,p2m2v]",
                type="string", default=None)
  opts, args = op.parse_args()
  if len(args):
    raise Exception("Problem with parameters!")
  else:
    if opts.maxmodelsize: maxmodelsize = opts.maxmodelsize
    if opts.models: models= opts.models
    if opts.conflicts: conflicts = opts.conflicts
    if opts.params:
      p_seq,p_xor,p_ior,p_and,p1m1v,p1m2v,p2m1v,p2m2v = \
        [float(p) for p in \
          opts.params.replace('[','').replace(']','').split(',')]

      if not (p_seq+p_xor+p_ior+p_and) == 1.0 \
      or not (p1m1v+p1m2v+p2m1v+p2m2v) == 1.0:
        raise Exception("Probabilities must sum to 1.0")
      else:
        p_structs = [p_seq, p_xor, p_ior, p_and]
        p_conflicts = [p1m1v, p1m2v, p2m1v, p2m2v]
    else:
      p_structs = p_conflicts = None

  CA = CPNAnalyser()

  # Stats header
  print()
  print("Mean", '', '', '', '', '', '', '', '', '', '', '', '',
        "Stdev", '', '', '', '', '', '', '', '', '',
        "Stderr", '', '', '', '', '', '', '', '', '',
        sep=',')
  print("Single Models", '', '', '', '', '', '', '', '', '', "Composed", '', '',
        "Single Models", '', '', '', '', '', '', "Composed", '', '',
        "Single Models", '', '', '', '', '', '', "Composed", '', '',
        sep=',')
  print("Activities", "Models", "States", "Conflicts",
        "Seq", "XOR", "IOR", "AND", "Secs", "Found", "States", "Secs", "Found",
        sep=sep, end=',')
  print("States", "Seq", "XOR", "IOR", "AND", "Secs", "Found",
        "States", "Secs", "Found", sep=sep, end=',')
  print("States", "Seq", "XOR", "IOR", "AND", "Secs", "Found",
        "States", "Secs", "Found", sep=sep)

  # Work up to max modelsize (may want to supply a set of model sizes)
  for modelsize in range(minmodelsize,maxmodelsize+1):
    # Accumulate stats
    seq, xor, ior, pll, cfs = [], [], [], [], []
    runtime, runtimeC, found, foundC, states, statesC = [], [], [], [], [], []
  
    # Number of samples
    for cc in range(models):
      print('.',end='')
      sys.stdout.flush()
      # Generate two random BPMN models
      B = [BPMNV(), BPMNV()]
      for ii in [0,1]:
        s, x, i, p = B[ii].gen_rnd(modelsize, p_structs)
        seq.append(s)
        xor.append(x)
        ior.append(i)
        pll.append(p)
    
      # Randomly add conflicts (return 3 numbers for model 1, 2, and joint
      cfs.append(rnd_conflict(B, conflicts, p_conflicts))
      sout = sys.stdout
    
      # Output the imported BPMNs
      fn = ["rnd1.bpmn", "rnd2.bpmn"]
      if output_bpmn:
        for ii in [0, 1]:
          dotfile = path.basename(fn[ii]).replace(".bpmn", '_bpmn.dot')
          epsfile = dotfile.replace(".dot", ".eps")
          f = open(dotfile, 'w')
          sys.stdout = f
          B[ii].print(ptype="dot")
          f.close()
          sys.stdout = sout
          cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
          system(cmd)
    
      # Convert to CPN
      C = [B[0].toCPN(), B[1].toCPN()]
    
      # Output the derived CPNs
      if output_cpn:
        for ii in [0, 1]:
          dotfile = path.basename(fn[ii]).replace(".bpmn", '_cpn.dot')
          epsfile = dotfile.replace(".dot", ".eps")
          f = open(dotfile, 'w')
          sys.stdout = f
          C[ii].print(ptype="dot")
          f.close()
          sys.stdout = sout
          cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
          system(cmd)
    
      # Reachability analysis for the two CPNs and their composition.
      bCompose = True
      f, fC, r, rC, s, sC = \
        CA.reachAnalysis(B, C, fn, bCompose, bPrint, log, output_cpn, output_rg)

      found.append(f)
      foundC.append(fC)
      runtime.append(r)
      runtimeC.append(rC)
      states.append(s)
      statesC.append(sC)


    ################# STATS
    # Merge stats for models 1 and 2
    # Bodge because we cannot compare the generated and discovered conflicts
    # correctly at present.  Basically, if the number of `discovered' conflicts
    # for single models exceeds the number recorded as generated, we set it to
    # 1 (for 100%).  Ditto for composed.
    for ii in range(models):
      if sum(found[ii]) >= sum(cfs[ii][0:2]):  # Individual models
        found[ii] = 1
      else:
        found[ii] = sum(found[ii])/sum(cfs[ii][0:2])
      if foundC[ii] >= cfs[ii][2]:  # Composed models
        foundC[ii] = 1
      else:
        foundC[ii] /= cfs[ii][2]
      cfs[ii] = sum(cfs[ii])  # Merge the conflict counts

    meanfound = np.mean(found)
    meancfs = np.mean(cfs)
    stdfound = np.std(found)
    stdcfs = np.std(cfs)
#   meanfound = np.mean([f[0] for f in found] + [f[1] for f in found])
#   meanfound = np.sum(found)/models
#   meanfoundsq = np.sum([sum(f)**2 for f in found])/models
#   meancfs = np.mean([c[0] for c in cfs] + [c[1] for c in cfs])
#   meancfs = np.sum(cfs)/models
#   meancfssq = np.sum([sum(c)**2 for c in cfs])/models
#   stdfound = np.std([f[0] for f in found] + [f[1] for f in found])
#   stdfound = np.sqrt(meanfoundsq - (meanfound**2))
#   stdcfs = np.sqrt(meancfssq - (meancfs**2))
#   stdcfs = np.std([c[0] for c in cfs] + [c[1] for c in cfs])
#   meanfound = min(meanfound, meancfs)  # Bodge for poor counting
    err = np.sqrt(models)
    print()
    print("%i,%i,%i,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f"
          % (modelsize, models, np.mean(states), conflicts,
             np.mean(seq), np.mean(xor), np.mean(ior), np.mean(pll),
             np.mean(runtime), meanfound,
             np.mean(statesC), np.mean(runtimeC), np.mean(foundC),
             np.std(seq), np.std(xor), np.std(ior), np.std(pll),
             np.std(states), np.std(runtime), stdfound,
             np.std(statesC), np.std(runtimeC), np.std(foundC),
             np.std(seq)/err, np.std(xor)/err, np.std(ior)/err, np.std(pll)/err,
             np.std(states)/err, np.std(runtime)/err, stdfound/err,
             np.std(statesC)/err, np.std(runtimeC)/err, np.std(foundC)/err))
  
