#!/usr/bin/env python2
# Phil Weber 2016-12-06 Experimental framework for BPMNV + CPN Analsyis.
# Load BPMN, convert to RG, optionally save both to dot, eps and pickle.
# Create tokenset to cover the CPN, pickle.
# Load pickles (as a test) and check for dead markings.

from __future__ import print_function
import sys
from os import path, devnull, system
import cPickle as pickle
from CPN import *
from BPMNV import *
import CPNParse
from CPNAnalysis import CPNAnalyser

sep = ','  # for output


if __name__ == "__main__":
  log = True
  timings = False
  output_bpmn = True
  output_cpn = True
  output_rg = False

  from optparse import OptionParser
  op = OptionParser()
  op.add_option('-f', '--filename', dest="fn",
                help="BPMN file to analyse",
                type="string", default=None)
  op.add_option('-d', '--deadmarkings', dest="deadfn",
                help="File containing dead markings (bespoke format)",
                type="string", default="")
  opts, args = op.parse_args()
  if len(args) or opts.fn is None:
    raise Exception("Problem with parameters!")
  else:
    deadfn = None
    if opts.fn: fn = opts.fn
    if opts.deadfn: deadfn = opts.deadfn

  CA = CPNAnalyser()

  # Initialise BPMN+V from file
  B = BPMNV(fn)
  sout = sys.stdout

  # Output the imported BPMN
  if output_bpmn:
    dotfile = path.basename(fn).replace(".bpmn", '_bpmn.dot')
    pklfile = dotfile.replace(".dot", ".pkl")
    epsfile = dotfile.replace(".dot", ".eps")
    pickle.dump(B, open(pklfile, "wb"))
    f = open(dotfile, 'w')
    sys.stdout = f
    B.print(ptype="dot")
    f.close()
    sys.stdout = sout
    cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
    system(cmd)

  # Convert to CPN
  C = B.toCPN()

  # Output the derived CPN
  if output_cpn:
    dotfile = path.basename(fn).replace(".bpmn", '_cpn.dot')
    pklfile = dotfile.replace(".dot", ".pkl")
    epsfile = dotfile.replace(".dot", ".eps")
    pickle.dump(C, open(pklfile, "wb"))
    f = open(dotfile, 'w')
    sys.stdout = f
    C.print(ptype="dot")
    f.close()
    sys.stdout = sout
    cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
    system(cmd)

  # Reachability analysis for the two CPNs and their composition.
  bPrint = True
  if deadfn:
    CA.reachAnalysisFile(B, C, path.basename(fn), deadfn, bPrint)
  else:
    bCompose = False
    f, fC, r, rC, s, sC = \
      CA.reachAnalysis(B, C, path.basename(fn),
                       bCompose, bPrint, log, output_cpn, output_rg)

