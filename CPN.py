#!/usr/bin/env python
# Phil Weber 2016-11-16

"""
Class for manipulating Coloured Petri Nets.
- import to internal representation.
- export to GraphViz dot, CPN/Tools (buggy and incomplete).
- convert to and from BPMN
- state space analysis (reachability graph).
- works on Windows under Cygwin.

Limitations:
  o  Export to CPN/Tools is buggy and incomplete (missing layout, colsets).
  o  Only tested on Python 2.6 (Linux), 2.7 (Windows Cygwin) due to availability
     of modules.
  o  Assuming only one token colour in net at any time (one patient): maybe
     multiple copies in a place if the WF Net has a badly-formed cycle.
  o  No inhibitor arcs.
  o  No attempt made yet for code efficiency (reachability graph analysis).
     *** PARTICULAR PROBABLE INEFFICIENCY OF notin() and equal() ***

Possibilities:
  o  Maybe var should be object (not dictionary).
  o  Make another attempt to split into per-class files.

History:
  o  2016-11-23: Fixed bug in finding variable definitions in CPN/Tools.
  o              Added partial colset export to CPN/Tools (from place datatypes,
                 but if bespoke, are not defined).
  o              Added export of variables to CPN/Tools.
  o  2016-12-02: Abstracted code to add{Place|Trans|Arc} and changed 'id'
                 identifiers.
  o  2016-12-05: Naive wrapping of text to aid display.
  o  2016-12-07: Initialise start and end places on __init__.
"""

from __future__ import print_function
from copy import deepcopy
from lxml import etree
from math import sqrt
import CPNParse

# debug = False
gr = CPNParse.parser()

# Graph: Set some defaults for dot
dg_rankdir    = "LR";      # Arcs left --> right by default
dg_ranksep    = 0.3;       # Min " between ranks
dg_fontname   = "Arial";
dg_fontsize   = 8;        # Points
dg_remincross = "true"; # dot internal
dg_margin     = "0.0.0.0";

# Edges
e_arrowsize   = 0.5;
e_fontname    = "Arial";
e_fontsize    = 8;

# Nodes
n_height      = 0.5;
n_width       = 0.5;
n_fontname    = "Arial";
n_fontsize    = 8;

TtoP = "TtoP"
PtoT = "PtoT"
ETA = "_eta"

### Token
class Token(object):
  def __init__(self, vdict):
    """ Create a token with the associated var dictionary (colset). """
    # Basic type checking
    self.vdict = {}

    if type(vdict) is not dict:
      raise Exception("Invalid variable dictionary for token.")
    else:
      for var in vdict.keys():
        if type(vdict[var]) is not dict \
        or len(vdict[var]) != 2:
          raise Exception("Invalid token tuple.")
    self.vdict = deepcopy(vdict)

  def add(self, vdict, replace=True):
    """ Add the {variable[type][value]) dictionary to the token dict.,
        optionally not replacing any keys already present.
    """
    for var in vdict:
      if replace or var not in self.vdict:
        self.vdict[var] = vdict[var]

  def print(self, noend=False):
    bFirst = True
    for v in self.vdict:
      if not bFirst:
        print("; %s=%s" % (v, str(self.vdict[v]["value"])), end='')
      else:
        print(" [%s=%s" % (v, str(self.vdict[v]["value"])), end='')
        bFirst = False
    print(']', end='')
    if not noend:
      print()

  def getlabel(self):
    s = ''
    for v in self.vdict:
      if s != '':
        s += "; "
      else:
        s += " ["
      s += v
      s += '='
      s += str(self.vdict[v]["value"])
    s += ']'
    return s


### Place
class Place(object):
  def __init__(self, pid, text, datatype):
    """ Define place attributes.
        text may be same as pid.
        datatype may be unnecessary (CPN/Tools colset).
        M is an optional list of tokens.
    """
    self.oid = pid
    self.text = text
    self.datatype = datatype

  def print(self):
    print("Place: %s (%s, %s)" % (self.text.replace("\n"," "), self.oid,
                           self.datatype))
    
### MarkedPlace
class MarkedPlace(Place):
  """ Inherit place and add marking. """
  def __init__(self, place):
    if type(place) is not Place:
      raise Exception("Invalid place")
    self.p = place
    self.M = []

  def produce_token(self, t, reason, M=None):
    """ Add a copy of the token to the place. """
    if type(t) is not Token:
      print("Cannot produce invalid token ", end='')
      t.print(True)
      print(" in place", self.text)
      raise Exception()
    else:
      self.M.append(t)
      return True

  def consume_token(self, token):
    """ Remove token from place. """
    if type(token) is not Token:
      print("Cannot consume invalid token ", end='')
      token.print()
      print("from place", self.text)
      raise Exception("Cannot consume invalid token.")
    else:
      bRemoved = False

      for t in self.M:
        if t == token:
          try:
            self.M.remove(token)
            bRemoved = True
          except Exception as e:
            raise e
      if not bRemoved:
        print("Token ", token, ' ', end='')
        token.print()
        print("not in place", self.text)
        raise Exception("Token not found in MarkedPlace", self)

  def marking(self):
    """ Return marking. """
    return self.M


### Transition
class Trans(object):
  def __init__(self, tid, text, guard):
    """ Define transition attributes.
        text may be same as tid.
        guard is mathematical expression; may be empty.
    """
    self.oid = tid
    self.text = text
    self.guard = guard

  def print(self, pretty=False, noend=False):
    if noend:
      end=''
    else:
      end="\n"
    if pretty:
      print(self.text.replace("\n"," "), end=end)
    else:
      print("Trans: %s (%s, %s)" % (self.text.replace("\n"," "),
            self.oid, self.guard))

  def evalGuard(self, vdict):
    """ Return evaluation (true/false) of this transition's guard. """
    bPassed = True
    if self.guard not in [None, ""]:
      guards = self.guard.split(',')
      for guard in guards:
        (var, op, val, neg) = gr.tguard.parse(guard, vdict)
        S = neg + ' ' + str(vdict[var]["value"]) + ' ' + op + ' ' + str(val)
        b = eval(S)
        bPassed = bPassed and eval(S)
    return bPassed

  def fire(self, cpn, T, M):
    """ Fire assuming token in each input place (already checked) and that they
        are all the same.
        - Consume token from each input place.
        - Create tokens (modify same object) on T's succeeding places.
        - Modify the tokens according to any arc transcriptions.
        - Return new marking M'.
    """
    # Remove token from each input place.
    t = M.token

    for p in cpn.preset(T):
      mp = M.P_MP[p]
      mp.consume_token(t)
      M.replace(mp)

    # Create token on each following place, modifying the tokens
    for f in cpn.postarcs(T):
      # Execute the arc inscription (if any) on the token.
      # NB we change the data on the Token, wanting it to affect all copies of
      #    the token in the net.
      if f.ann is not None:
        inscrs = gr.arcins.parse(f.ann)
        t.vdict = gr.arcins.updval(t.vdict, inscrs)
      post_p = f.post()
      M.addMP(post_p, t)
      M.sortMP

    return M


### Arc (PN)
class Arc(object):
  def __init__(self, aid, fr, to, ann):
    """ Define arc attributes.
        fr and to are P or T objects, not explicitly defined as place/trans.
        annotation (ann) is mathematical expression; may be empty.
    """
    self.oid = aid
    self.fr = fr
    self.to = to
    self.ann = ann

  def evalInscr(self, gr, vdict):
    """ Execution of the inscription on this arc. """
    if self.ann not in [None, ""]:
      results = gr.arcins.parse(self.ann)
      vdict = gr.arcins.updval(vdict, results)
    return vdict

  def pre(self):
    return self.fr

  def post(self):
    return self.to

  def print(self):
    print("Arc: (%s -> %s [%s])" % (self.fr.text.replace("\n"," "),
                               self.to.text.replace("\n"," "), self.ann))
  

### Marking
class Marking(object):
  def __init__(self, MP):
    # Token used in this marking.  Stored in order to be able to copy it when
    # copying marking.  One token per marking; we want its data to change
    # globally for the instance - but multiple copies could exist in the case of
    # a badly-formed Workflow Model.

    """ Initialise marking as a list of (marked) place objects. """
    self.P_MP = {}
    for mp in MP:
      if type(mp) is not MarkedPlace:
        raise Exception("Marking invalid")
      else:
        # Places in underlying CPN do not know their corresp. marked places,
        # so create a map.  This is rather ugly.
        self.P_MP[mp.p] = mp
    self.MP = MP
    # 1st Token in 1st MarkedPlace in Marking (should be only Token)
    self.token = MP[0].marking()[0]

  def markedplaces(self):
    """ Return list of marked places (places with token lists). """
    return self.MP

  def replace(self, mp):
    """ Replace the marked place (???) (assuming new marking). """
    if mp in self.MP:
      self.MP.remove(mp)
      del(self.P_MP[mp.p])
    if mp.marking() not in [[], None]:
      self.MP.append(mp)
      self.P_MP[mp.p] = mp

  def addMP(self, p, t):
    if p in self.P_MP:
      mp = self.P_MP[p]
    else:
      mp = MarkedPlace(p)
      self.P_MP[p] = mp
    mp.produce_token(t, "add")
    self.replace(mp)  # duplication?

  def copy(self):
    """ Create a new marking object.  Create a new MarkedPlace object for each
        existing MarkedPlace Object, keeping the Place object references, but
        creating new Tokens.
    """
    M = None
    newMP = []
    newt = Token(self.token.vdict)
    for mp in self.MP:
      newmp = MarkedPlace(mp.p)
      for t in mp.M:
        newmp.produce_token(newt, "copy")
      newMP.append(newmp)
    M = Marking(newMP)
    return M

  def sortMP():
    """ Sort MP list to make comparison easier. """
    self.MP = sorted(self.MP)

  def print(self, deep=False, pretty=False, noend=False, indent=0):
    if pretty:
      bFirst = True
    for mp in self.MP:
      p = mp.p
      if pretty:
        if not bFirst:
          print(", ", end='')
        else:
          print(' ' * indent, end='')
          print('(', end='')
          bFirst = False
        print(p.text, end='')
      else:
        print("  ", p.text, p, mp, mp.marking())
        if deep:
          for t in mp.marking():
            t.print(noend=True)
    if pretty:
      print(')', end='')
    if not pretty or not noend:
      print()

  def getlabel(self):
    s = ''
    for mp in self.MP:
      p = mp.p
      if s != '' :
        s += ", "
      else:
        s += '('
      s += p.text
    s += ')'
    return s

  def enabled_trans(self, cpn):
    """ Return a list of (token, transition) pairs indicating which transitions
        are enabled in this marking, and the tokens which enable them.
        M is a list of (Marked) Places.
        - Identify all enabled transitions by taking t, finding the output
          transtions of the place p containing it, checking that T's guard
          is satisfied by t, checking that each input place contains the
          same token.
          Note: we ignore issues around infinite markings.
    *** Some thinking to do here.  May need to pass back (token, trans) pair.
        Need to not duplicate work between this and the firing.
    """
    ET = []  # Enabled transitions
    CT = []  # Checked transitions
    for mp in self.MP:
      p = mp.p
      tprint = mp.marking()[0]
      for t in mp.marking():  # token array
        for T in cpn.postset(p):  # postset of transitions
          # Is the guard satisfied?
          if T not in CT:
            if T.evalGuard(t.vdict):
              # Do all the places in the preset of T contain t? 
              bPostTEnabled = True
              for pre_place in cpn.preset(T):
                if pre_place is not p:
                  if pre_place not in self.P_MP \
                  or t not in self.P_MP[pre_place].marking():
                    bPostTEnabled = False
              if bPostTEnabled:
                if T not in ET:
                  ET.append(T)
            CT.append(T)
    return ET

  def equal(self, M, S=None):
    """ Test equivalence of two markings according to their underlying places
        and tokens.  *** THIS MAY BE VERY INEFFICIENT ***
    """
    # Allow S to be passed in (for calling from notin()).
    if S is None:
      S = []
      for mp_self in self.MP:
        for t in mp_self.M:
          v = []
          for var in sorted(t.vdict.keys()):
            v.append(var)
            for k in sorted(t.vdict[var].keys()):
              v.append(k)
              v.append(t.vdict[var][k])
        S.append(v)

    S1 = []
    for mp_M in M.MP:
      for t in mp_M.M:
        v = []
        for var in sorted(t.vdict.keys()):
          v.append(var)
          for k in sorted(t.vdict[var].keys()):
            v.append(k)
            v.append(t.vdict[var][k])
      S1.append(v)

    # Check same places are marked.  Unsure whether to trust this.
    if sorted(self.P_MP.keys()) == sorted(M.P_MP.keys()):
      # Check same number of marked places 
      if len(self.MP) == len(M.MP):
        # Check each token in each of the MarkedPlaces exists in the other
        # Marking: manually because the objects are different.
        if S1 != S:
          return False
        else:
          return True
    

  def notin(self, markingList):
    """ Check whether this marking is in the list: check each place and token
        equality.  *** THIS MAY BE VERY INEFFICIENT ***
    """
    S = []
    for mp_self in self.MP:
      for t in mp_self.M:
        v = []
        for var in sorted(t.vdict.keys()):
          v.append(var)
          for k in sorted(t.vdict[var].keys()):
            v.append(k)
            v.append(t.vdict[var][k])
      S.append(v)

    bFoundM = False  # Set when self Marking is found in MarkingList
    for M in markingList:  # Check each Marking in P
      if self.equal(M, S):
        return M
    
    # Here we need true/false on bFoundM
    # True if one M found equivalent to self.    
    return None


### Reachability Graph
class RG(object):
  def __init__(self):
    """ Reachability Graph: Note: states are markings. """
    self.S = [] # States (Markings)
    self.E = [] # Edges
    self.dead = []

  def addState(self, M):
    # sid = len(S)
    # s = RGState(M)
    M_ = M.notin(self.S)  # Checks if equivalent exists
    if M_ is None:
      self.S.append(M)
      M_ = M
    return M_

  def addEdge(self, M, T, M_):
    if M not in self.S or M_ not in self.S:
      raise Exception("Cannot add edge between non-existent states!")
    e = Edge(M, T, M_)
    self.E.append(e)

  def print(self, ptype=None, indent=0):
    """ Pretty output in internal representation format or GraphViz dot.
    """
    if not ptype == "dot":
      for m in self.S:
        print(' ' * indent, end='')
        print("State:", end=' ')
        m.print(pretty=True, noend=True)
        m.token.print()
      for e in self.E: e.print(indent=indent)

    elif ptype == "dot":
      # Output graph header, defaults etc.
      print_dot_header("RG")

      # Output states, marking states indicating dead markings.
      sid = 0
      for m in self.S:
        label = wrap(m.getlabel())
        xlabel = wrap(m.token.getlabel())
        print('  %s [shape="circle",label="%s"' % (sid, label), end='')
        print(', xlabel="%s"' % xlabel, end='')
        mdead = m.notin(self.dead)
        if mdead is not None:
          # print("style=filled,fillcolor=red", end='')
          print("color=red,fontcolor=red", end='')
        print("];")
        sid += 1
  
      # Output edges
      for e in self.E:
        fr = self.S.index(e.fr)
        to = self.S.index(e.to)
        label = wrap(e.T.text.replace("\n","\\n"))
        print('  %s -> %s [label="%s"];' % (fr, to, label));

      # Print graph trailer.
      print("}")


### RG State
class RGState(object):
  def __init__(self, sid, M):
    """ Reachability graph state.  Unique ID.
        Stores Marking which allows label to be created from the underlying
        place names and tokens contained in the Marking (list of MarkedPlaces)
    """
    self.oid = sid
    self.M = M
    self.P = []  # List of underlying places
    self.dead = None

    
### RG Edge
class Edge(object):
  def __init__(self, fr, T, to):
    self.T = T  # PN Transition executed on this edge
    self.fr = fr  # RG state (marking)
    self.to = to  # RG state (marking)

  def print(self, indent=0):
    print(' ' * indent, end='')
    print("Edge: ", end='')
    self.fr.print(pretty=True, noend=True, indent=0)
    self.fr.token.print(noend=True)

    print(" -- ", end='')
    self.T.print(noend=True, pretty=True)
    print(" --> ", end='')

    self.to.print(pretty=True, noend=True, indent=0)
    self.to.token.print()


### Top-level CPN
class CPN(object):
  validCPNVarTypes = ["BOOL", "INT", "REAL", "STRING"]

  ### CPN definitions
  def __init__(self, fn=None):
    """ Set up empty CPN structure, optionally populating from file.
    """
    self.CPNvars = {} # Variable definitions {var: {type, value}} # val unused
    self.P0 = None # Start places
    self.Pe = None # End places
    self.P = [] # Places
    self.T = [] # Transitions
    self.F = [] # Arcs
    if fn:
      self.parse_cpn(fn)
    self.cover_token_set = [] # List of tokens to explore the net
    # [token1 = [(var1,val1),(var2,val2),...] (token obj)
    #  token2 = [(var1,val3),(var2,val2),...] (pair list could be dict)
    #  token3 = [(var1,val1),(var2,val5),...]
    # ...
    # ]

  def addPlace(self, pid, ptext, datatype=None):
    p = Place(pid, ptext, datatype)
    self.P.append(p)
    return p

  def addTrans(self, tid, ttext, guard=None):
    t = Trans(tid, ttext, guard)
    self.T.append(t)
    return t

  def addArc(self, aid, orient, tend, pend, ann=None):
    """ Note: tend and pend are the object "oid"s, not the objects themselves.
    """
    # find the objects corresponding to tend and pend
    Op = [p for p in self.P if p.oid == pend]
    Ot = [t for t in self.T if t.oid == tend]
    if len(Op) > 1 or len(Ot) > 1:
      print(aid, orient, tend, pend)
      raise SystemError("Multiple arc end points!")
    elif len(Op) < 1 or len(Ot) < 1:
      raise SystemError("Missing arc end points!")
    else:
      (Op, Ot) = Op[0], Ot[0]

    # Check for existence of arc (e.g. result of adding eta transitions) before
    # adding.
    bArcExists = False
    if orient == PtoT:
      for post in self.postarcs(Op):
        if post.to == Ot:
          bArcExists = True
      if not bArcExists:
        f = Arc(aid, Op, Ot, ann)
    elif orient == TtoP:
      for post in self.postarcs(Ot):
        if post.to == Op:
          bArcExists = True
      if not bArcExists:
        f = Arc(aid, Ot, Op, ann)
    else:
      raise SystemError("Unexpected arc direction in CPN import!")

    if not bArcExists:
      self.F.append(f)
    else:
      f = None
    return f

  def __call__(self):
    return self.print()

  def __get__(self):
    return self.print()

  def print(self, ptype=None):
    """ Pretty output in internal representation format, GraphViz dot or
        CPN/Tools XML - buggy and at present no layout (perhaps could render
        a layout e.g. using GraphViz dot to xdot format, then read that),
        or colour set and variable etc. definitions.
    """
    if ptype not in ["dot", "CPN/Tools"]:
      for v in self.CPNvars: print("var", v, ':', self.CPNvars[v]["type"],
                                   self.CPNvars[v]["value"])
      for p in self.P: p.print()
      for t in self.T: t.print()
      for f in self.F: f.print()
      for t in self.cover_token_set:
        print("token: ", end='')
        t.print()

    elif ptype == "dot":
      # Output graph header, defaults etc.
      print_dot_header("CPN")

      # Output places (no markings at present).
      for p in self.P:
        label = wrap(p.text.replace('\n','\\n'))
        xlabel = ''
        if p.datatype is not None:
          label = wrap(label + "\\n[" + p.datatype + ']')
        if p in self.start_places():
          for var in self.CPNvars.keys():
            xlabel += "\\n" + var + ": " + self.CPNvars[var]["type"]
        print('  %s [shape="circle",label="%s",xlabel="%s"' % 
          (p.oid, label, xlabel), end='')
        print("];")
  
      # Output transitions with guards
      for t in self.T:
        # Deal with "hidden" transitions"?
        label = wrap(t.text.replace("\n","\\n"))
        if t.guard is not None:
          label = wrap(label + "\\n" + '[' + t.guard + ']')
        print('  %s [shape="box",label="%s"' % 
          (t.oid, label))
        if t.text.startswith(ETA):
          print("style=filled,fillcolor=black,fontcolor=white,width=0.1")
        print('];')
  
      # Output arcs
      for f in self.F:
        if f.ann is None:
          ann = ''
        else:
          ann = f.ann
        print('  %s -> %s [label="%s"];' % (f.fr.oid, f.to.oid, ann));

      # Print graph trailer.
      print("}")
    elif ptype == "CPN/Tools":
      ii_id = 0

      ws  = etree.Element("workspaceElements")
      gen = etree.SubElement(ws, "generator", tool="CPN Tools", version="4.0.1",
              format="6")
      cpn = etree.SubElement(ws, "cpnet")
      glb = etree.SubElement(cpn, "globbox")

      # Standard declarations: colsets: partial; bespoke colsets lifted from
      # place definitions have no definition.
      blk = etree.SubElement(glb, "block", id="ID1")
      bid = etree.SubElement(blk, "id")
      bid.text = "Standard declarations"
      colors = ["UNIT", "INT", "BOOL", "STRING"]
      for p in self.P:
        if p.datatype not in ['', None]:
          if p.datatype not in colors:
            colors.append(p.datatype) 
      for color in colors:
        colid = "ID" + str(ii_id)
        ii_id += 1
        col = etree.SubElement(blk, "color", id=colid)
        cid = etree.SubElement(col, "id")
        cid.text = color
        unit = etree.SubElement(col, color.lower())

      # Possible limitation: specifies each individually, not grouped by type.
      for v in self.CPNvars:
        varid = "ID" + str(ii_id)
        ii_id += 1
        var = etree.SubElement(blk, "var", id=varid)
        typ = etree.SubElement(var, "type")
        tid = etree.SubElement(typ, "id")
        tid.text = self.CPNvars[v]["type"]
        vid = etree.SubElement(var, "id")
        vid.text = v

      pageid = "ID" + str(ii_id)
      ii_id += 1
      page = etree.SubElement(cpn, "page", id=pageid)
      pageattr = etree.SubElement(page, "pageattr", name="CPN Export")

      for p in self.P:
        place = etree.SubElement(page, "place", id=p.oid)
        fillattr = etree.SubElement(place, "fillattr", colour="White",
                     pattern="", filled="false")
        lineattr = etree.SubElement(place, "lineattr", colour="Black",
                     thick="1", type="Solid")
        textattr = etree.SubElement(place, "textattr", colour="Black",
                     bold="false")
        txt   = etree.SubElement(place, "text")
        txt.text = p.text
        ellipse = etree.SubElement(place, "ellipse", w="60.0", h="40.0")

        typid = "ID" + str(ii_id)
        ii_id += 1
        typ   = etree.SubElement(place, "type", id=typid)
        fillattr = etree.SubElement(typ, "fillattr", colour="White",
                     pattern="Solid", filled="false")
        lineattr = etree.SubElement(typ, "lineattr", colour="Black",
                     thick="0", type="Solid")
        textattr = etree.SubElement(typ, "textattr", colour="Black",
                     bold="false")
        txt   = etree.SubElement(typ, "text", tool="CPN Tools", version="4.0.1")
        txt.text = p.datatype

      for t in self.T:
        trans = etree.SubElement(page, "trans", id=t.oid, explicit="false")
        fillattr = etree.SubElement(trans, "fillattr", colour="White",
                     pattern="", filled="false")
        lineattr = etree.SubElement(trans, "lineattr", colour="Black",
                     thick="1", type="Solid")
        textattr = etree.SubElement(trans, "textattr", colour="Black",
                     bold="false")
        txt   = etree.SubElement(trans, "text")
        txt.text = t.text
        box = etree.SubElement(trans, "box", w="60.0", h="40.0")

        condid = "ID" + str(ii_id)
        ii_id += 1
        cond  = etree.SubElement(trans, "cond", id=condid)
        if t.guard is not None:
          fillattr = etree.SubElement(cond, "fillattr", colour="White",
                       pattern="Solid", filled="false")
          lineattr = etree.SubElement(cond, "lineattr", colour="Black",
                       thick="0", type="Solid")
          textattr = etree.SubElement(cond, "textattr", colour="Black",
                       bold="false")
          txt = etree.SubElement(cond, "text", tool="CPN Tools", 
                  version="4.0.1")
          txt.text = t.guard

      for a in self.F:
        if a.fr in self.P:
          orient, transid, placeid = TtoP, a.to.oid, a.fr.oid
        else: 
          orient, transid, placeid = PtoT, a.fr.oid, a.to.oid
 
        arc = etree.SubElement(page, "arc", id=a.oid, orientation=orient,
                order="1")
        fillattr = etree.SubElement(arc, "fillattr", colour="White",
                     pattern="", filled="false")
        lineattr = etree.SubElement(arc, "lineattr", colour="Black",
                     thick="1", type="Solid")
        textattr = etree.SubElement(arc, "textattr", colour="Black",
                     bold="false")
        arrattr  = etree.SubElement(arc, "arrowattr", headsize="1.2",
                     currentcyckle="2")

        transend = etree.SubElement(arc, "transend", idref=transid)
        placeend = etree.SubElement(arc, "placeend", idref=placeid)
        annid = "id" + str(ii_id)
        ii_id += 1
        ann = etree.SubElement(arc, "annot", id=annid)
        if a.ann is not None:
          txt = etree.SubElement(ann, "text", tool="CPN Tools", version="4.0.1")
          txt.text = a.ann
          fillattr = etree.SubElement(arc, "fillattr", colour="White",
                       pattern="Solid", filled="false")
          lineattr = etree.SubElement(arc, "lineattr", colour="Black",
                       thick="0", type="Solid")
          textattr = etree.SubElement(arc, "textattr", colour="Black",
                       bold="false")

      insts = etree.SubElement(cpn, "instances")
      inst = etree.SubElement(insts, "instance", id="ID0", page=pageid)
      binds = etree.SubElement(cpn, "binders")
      cpnb = etree.SubElement(binds, "cpnbinder", id="ID0", x="100", y="100",
               width="800", height="600")
      sh = etree.SubElement(cpnb, "sheets")
      cpnsh = etree.SubElement(sh, "cpnsheet", id="ID0", panx="-10.0",
                pany="10.0", zoom="1.0", instance="ID0")
      cpnsh.text = ""
 
      # Create the header manually
      print('<?xml version="1.0" encoding="iso-8859-1"?>')
      print('<!DOCTYPE workspaceElements PUBLIC ', end='')
      print('"-//CPN//DTD CPNXML 1.0//EN" "http://cpntools.org/DTD/6/cpn.dtd">')
      print()

      print(etree.tostring(ws, pretty_print=True))


  def parse_cpn(self, fn):
    """ Import CPN: currently assumes CPN/Tools format.
        Could extend to bespoke format similar to the output representation.
    """
    try: tree = etree.parse(fn)
    except IOError: raise SystemError("Problem opening", fn, "!")

    root = tree.getroot()

    # Variable definitions: ignoring colour sets and only recognising a subset
    # of types.
    B = root.find("cpnet").find("globbox").findall("block")
    ii = 0
    V = B[ii].findall("var")
    while ii < len(B) and len(V) <= 0:
      ii += 1
      V = B[ii].findall("var")
    for v in V:
      # 1.  Construct a string for parsing by the parser!
      t = v.find("type").find("id").text
      if t in self.validCPNVarTypes:
        vs_init = "var "
        varstring = vs_init
        vids = v.findall("id")
        for vid in vids:
          if not varstring == vs_init:
            varstring += ", "
          varstring += vid.text
        varstring += ": " + t

      # 2.  Parse the string
      (vv, vtype) = gr.vardef.parse(varstring)
      for var in vv:
        self.CPNvars[var] = {"type": vtype, "value": None}

    # Place defintions (with optional colour sets - unused)
    P = root.find("cpnet").find("page").findall("place")
    for p in P: 
      pid = p.attrib["id"]
      ptext = p.find("text").text
      datatype = p.find("type").find("text").text
      self.addPlace(pid, ptext, datatype)

    # Transitions (with optional guards)
    T = root.find("cpnet").find("page").findall("trans")
    for t in T:
      tid = t.attrib["id"]
      ttext = t.find("text").text
      guard = t.find("cond").find("text").text
      self.addTrans(tid, ttext, guard)

    # Arcs
    A = root.find("cpnet").find("page").findall("arc")
    for a in A:
      aid = a.attrib["id"]
      orient = a.attrib["orientation"]
      tend = a.find("transend").attrib["idref"]
      pend = a.find("placeend").attrib["idref"]
      ann = a.find("annot").find("text").text
      self.addArc(aid, orient, tend, pend, ann)

  # def import_pnml(self, fn):

  def cover_tokens(self):
    """ Build a set of tokens in the start place that will cover all the
        guard decisions, i.e. allow exploration of the full net.

        NB: This is quite naive: does it really explore the full net?
    """
    # For all guards:
    #   parse the guard, extract the variable x, negation, op, val
    #   if 'x' or "not x":
    #     create tokens for x in [True, False]
    #   if "x op val":
    #     if >= or <, need token values (a,b) = (val-1, val) for x
    #     if > or <=, need token values (a,b) = (val, val+1) for x
    #   if no tokens, create 2 tokens with just x, values a and b
    #   if tokens, add x, duplicating all tokens for x = a and b

    # 1. build a dictionary of variables to value pairs.
    var_vals = {}  # {var: [v1, v2]}
    self.cover_token_set = []
    for t in self.T:
      checkvals = gr.tguard.guard_testvals(t.guard, self.CPNvars)
      if not checkvals == []:  # No parsed guard(s)
        for ck in checkvals:  # (var, (range pair)) tuple
          var = ck[0]  # string
          ck = ck[1]  # pair
          for v in ck:
            if var not in var_vals:
              var_vals[var] = [v]
            else:
              if v not in var_vals[var]:
                var_vals[var].append(v)

    # TBC Add proper checking that all variables in the net are defined.
    if len(var_vals.keys()) == 0:
      # Dummy token
      self.cover_token_set = [Token({'i':{"type": "INT", "value": 0}})]
    else:
      # 2. build tokens of unique combinations.
      for var in var_vals.keys():
        vals = var_vals[var]
        if len(self.cover_token_set) == 0:  # no tokens created yet
          for v in vals:
            vtype = self.CPNvars[var]["type"]
            tokendict = {var: {"type": vtype, "value": v}}
            t = Token(tokendict)
            self.cover_token_set.append(t)  # should be object?
        else:
          newtokens = []  # Not efficient and perhaps should be recursive.
          for t in self.cover_token_set:
            for v in vals:
              newt = deepcopy(t)
              vtype = self.CPNvars[var]["type"]
              tokendict = {var: {"type": vtype, "value": v}}
              newt.add(tokendict)
              newtokens.append(newt)
          self.cover_token_set = newtokens
    return(self.cover_token_set)

  def rm_superfluous_eta(self):
    """ Remove useless eta-transitions: if single pre- and post-arc,
        preceding place has single post-arc, succeeding place has single
        pre-arc.
        NB: This could be extended, e.g. to remove redundant etas used as AND splits.
    """
    T_rm = []  # Save trans to remove after loop
    for t in self.T:
      if t.text.startswith(ETA):
        pre = self.preset(t)
        post = self.postset(t)

        if len(pre) == 1 and len(post) == 1:
          # Single pre- and post-places
          preplace = pre[0]
          postplace = post[0]
          preplace_prearcs = self.prearcs(preplace)
          preplace_postarcs = self.postarcs(preplace)
          postplace_prearcs = self.prearcs(postplace)
          postplace_postarcs = self.postarcs(postplace)

          if len(preplace_prearcs) == 1 \
          and len(preplace_postarcs) == 1:
            # Remove preplace and eta
            eta_postarc = self.postarcs(t)[0]
            preplace_prearcs[0].to = eta_postarc.to
            self.P.remove(preplace)
            T_rm.append(t)
            self.F.remove(preplace_postarcs[0])
            self.F.remove(eta_postarc)

          elif len(postplace_prearcs) == 1 \
          and len(postplace_postarcs) == 1:
            # Remove eta and postplace
            eta_prearc = self.prearcs(t)[0]
            postplace_postarcs[0].fr = eta_prearc.fr
            T_rm.append(t)
            self.P.remove(postplace)
            self.F.remove(eta_prearc)
            self.F.remove(postplace_prearcs[0])
          else:
            # Do nothing
            None

    for t in T_rm:
      self.T.remove(t)
    return len(T_rm)

  def preset(self, node):
    """ Return a list of places/transitions: the preset of this p/t. """
    pre = []
    for f in self.F:
      if f.post() == node and f.pre() not in pre:
        pre.append(f.pre())
    return pre

  def postset(self, node):
    """ Return a list of places/transitions: the postset of this p/t. """
    post = []
    for f in self.F:
      if f.pre() == node and f.post() not in post:
        post.append(f.post())
    return post

  def prearcs(self, node):
    """ Return a list of arcs arriving at this p/t (merge with preset?). """
    prearcs = []
    for f in self.F:
      if f.post() == node and f not in prearcs:
        prearcs.append(f)
    return prearcs

  def postarcs(self, node):
    """ Return a list of arcs leaving this p/t (merge with postset?). """
    postarcs = []
    for f in self.F:
      if f.pre() == node and f not in postarcs:
        postarcs.append(f)
    return postarcs

  def start_places(self):
    """ Return a list of start places; although we only allow one. """
    if self.P0 is None:
      self.P0 = []
      for p in self.P:
        if len(self.preset(p)) == 0:
          self.P0.append(p)
    return self.P0

  def end_places(self):
    """ Return a list of end places; although we only allow one. """
    if self.Pe is None:
      self.Pe = []
      for p in self.P:
        if len(self.postset(p)) == 0:
          self.Pe.append(p)
    return self.Pe


def wrap(s):
  """ Naive heuristic wrapping of string to try to improve dot display. """
  minwrap = 10
  if type(s) is str and len(s) > minwrap:
    n = int(sqrt(len(s)) * 2)
    s_ = '\\n'.join([s[ii:ii+n] for ii in range(0, len(s), n)])
    return s_
  else:
    return s

def print_dot_header(s):
  print('digraph ' + s + ' {')
  print('  forcelabels=true;')
  print('  ranksep="%s"; fontsize="%s"; remincross=%s; '
        % (dg_ranksep, dg_fontsize, dg_remincross))
  print('margin="%s"; fontname="%s";rankdir="%s";'
        % (dg_margin, dg_fontname, dg_rankdir))

  print('  edge [arrowsize="%s", fontname="%s",fontsize="%s"];' %
    (e_arrowsize, e_fontname, e_fontsize))

  print('  node [height="%s",width="%s",fontname="%s",'
        % (n_height, n_width, n_fontname))
  print('fontsize="%s"];'
        % (n_fontsize))
  return None
