# README #

Code relating to the experimentation and case study for the paper "Automated
Conflict Detection Between Medical Care Pathways" submitted to the Journal of
Software Evolution and Process (JSEP), Special Issue: Software Engineering for
Connected Health (Dec 2016).

### What is this repository for? ###

* Python code in support of the JSEP submission -- experimentation and case
study.  Caveat: this is rough code; none of it has been optimised for good
Python style, efficiency, or to work beyond the bounds of the experiments
explicitly carried out for this paper.  The current reachability analysis is
extremely inefficient.
* 0.1

### How do I get set up? ###

* Python front-end scripts:
    * BPMNVanalysis.py # Reachability analysis on one file.
    * BPMNVcompose.py  # Reachability analysis on two files plus their parallel
                       composition.
    * BPMNVrandom.py   # Reachability analyses and stats for timing and recovery
                       of conflicts for multiple random files.  The recovery
                       stats 'found' are rather lacking.

* Python modules
    * BPMNV.py         # BPMN+V (BPMN+data) models
    * CPN.py           # Coloured Petri Net models
    * CPNAnalysis.py   # Analysis (e.g. reachability)
    * CPNParse.py      # Parsing of basic grammar for guards and arc inscriptions

* Files containing lists of tokens and dead markings manually transcribed
from CPN/Tools reachability analysis.
    * casestudyOA_DMs         # From CPN/Tools - model 1
    * casestudyCOPD_DMs       # - model 2
    * casestudyOA_COPD_DMs    # - composition


* Experimental results
    * basic_models.out  # results from conflict detection in toy models
    * random\*.out      # results from conflict detection in random models
    * caseStudy\*.out   # results from conflict detection in case study

* Configuration

N/A

* Dependencies
Tested under Python 2.7.
LXML (http://lxml.de).

### Contribution guidelines ###

N/A.

### Who do I talk to? ###

* Phil Weber, School of Computer Science, University of Birmingham.2016-12-15

