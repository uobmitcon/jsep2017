#!/usr/bin/env python2
# Phil Weber 2016-12-07 Class for CPN Analysis (state space, composition, etc.).

""" Limitations:
    o  Checking marking is not in markinglist (CPN.notin()) is very inefficient.
    o  Reporting of which markings have been found.
"""

from __future__ import print_function
import sys
import time
from os import path, devnull, system
from copy import copy
from CPN import *
from BPMNV import *
import CPNParse

gr = CPNParse.parser()
sep = ','

### Class for analysis of CPN objects
class CPNAnalyser(object):
  def __init__(self):
    None

  def buildRG(self, cpn, token, log=False, timings=False):
    """ - ID start place.
        - Add marking to "unprocessed markings" U
        - Until U empty:
          - Remove unprocessed marking M.
          - For each transition T enabled by a given token t:
            - Fire T using t and associated (input) tokens, consuming them.
              - Create tokens on T's succeeding places.
              - Modify the tokens according to any arc transcriptions.
              - Add new marking M' to U if M' not in "processed markings" P.
          - Add M to P.
    
    """
    if type(token) is not Token:
      raise Exception("Cannot find RG from invalid token", token)

    U = [] # Unprocessed markings
    P = [] # Processed markings
    # dead = []

    # Find start and end places.
    P0 = cpn.start_places()
    Pe = cpn.end_places()
    if len(P0) > 1 or len(Pe) > 1:
      raise Exception("We only allow for single start and end places.")
    else:
      MP0 = MarkedPlace(P0[0])

    # Add token to create the initial marking.
    MP0.produce_token(token, "init")
    M0 = Marking([MP0])

    U.append(M0) # So original marking is not affected

    rg = RG()
    rg.inittoken = deepcopy(token)  # save for later analysis
    rg.addState(M0)

    # Process all unprocessed markings
    while not U == []:
      M = U.pop()
      if log:
        if timings:
          timei = time.time()
        else:
          print('.', end='')
          sys.stdout.flush()
      Madd = rg.addState(M)  # In case we reach a duplicate state

      ET = M.enabled_trans(cpn)

      if ET == []:
        rg.dead.append(Madd)

      for T in ET:
        # Fire trans T with token t
        M_ = M.copy()  # Copy the marking, taking care which objects keep.
        M_ = T.fire(cpn, T, M_)
        if Madd is None:
          Madd = M
        M_add = rg.addState(M_)
        rg.addEdge(Madd, T, M_add)
        if M_.notin(P) is None:  # Append for processing if not already done.
          U.append(M_)           # *** THIS IS VERY INEFFICIENT ***
      if M.notin(P) is None:  # Should never be?
        P.append(M)

      if log and timings:
        timeo = time.time()
        print(timeo - timei)
        sys.stdout.flush()

    # Debug printout at end.
    if log:
      print()
      print(str(len(P)), "states explored.")

    return (rg, len(P))

  def compose(self, cpn1, cpn2):
    """ Create new CPN.
        Copy CPNvars, P0, Pe, P, T, F (and?) from both, modifying names.
        Join with new start node and eta-transition.
        Merge the variables.

        NB: This will change the original CPNs (node names).  Ideally we need to
            copy all objects, preserving the structure.

        Test visualsation and RG analysis. ###
    """
    if len(cpn1.P0) > 1 or len(cpn1.Pe) > 1 \
    or len(cpn2.Pe) > 1 or len(cpn2.Pe) > 1:
      raise Exception(
        "Cannot compose CPNs with more than one start or end place")

    C = CPN()

    # Copy structure
    for p in cpn1.P:
      p.text = p.text + "_cpn1"  # Warning: changes the original!
      p.oid = str(p.oid) + "_cpn1"
      C.P.append(p)
    for p in cpn2.P:
      p.text = p.text + "_cpn2"
      p.oid = str(p.oid) + "_cpn2"
      C.P.append(p)
 
    for t in cpn1.T:
      t.text = t.text + "_cpn1"
      t.oid = str(t.oid) + "_cpn1"
      C.T.append(t)
    for t in cpn2.T:
      t.text = t.text + "_cpn2"
      t.oid = str(t.oid) + "_cpn2"
      C.T.append(t)
 
    for f in cpn1.F:
      f.oid = str(f.oid) + "_cpn1"
      C.F.append(f)
    for f in cpn2.F:
      f.oid = str(f.oid) + "_cpn2"
      C.F.append(f)

    # Merge variables
    for var in cpn1.CPNvars:
      C.CPNvars[var] = cpn1.CPNvars[var]  # NB value is object
    for var in cpn2.CPNvars:
      if var not in C.CPNvars:
        C.CPNvars[var] = cpn2.CPNvars[var]  # NB value is object
      elif cpn2.CPNvars[var]["type"] != C.CPNvars[var]["type"]:
        raise Exception("Incompatible types (", cpn2.CPNvars[var]["type"] + ','
                      + C.CPNvars[var]["type"] + ") for", var, "in merge.")

    # Join nets with single place and eta transition at start and end, to allow
    # the two original nets to run in parallel.
    s = C.addPlace("P0_compos", "P0")
    e = C.addPlace("Pe_compos", "Pe")
    eta_s = C.addTrans("_eta_s_compos", "_eta_s_compos")
    eta_e = C.addTrans("_eta_e_compos", "_eta_e_compos")
    f = C.addArc("compos_s0", PtoT, eta_s.oid, s.oid)
    f = C.addArc("compos_s11", TtoP, eta_s.oid, cpn1.P0[0].oid)
    f = C.addArc("compos_s12", TtoP, eta_s.oid, cpn2.P0[0].oid)
    f = C.addArc("compos_e0", TtoP, eta_e.oid, e.oid)
    f = C.addArc("compos_e11", PtoT, eta_e.oid, cpn1.Pe[0].oid)
    f = C.addArc("compos_e12", PtoT, eta_e.oid, cpn2.Pe[0].oid)

    # Set start and end place
    C.P0 = [s]
    C.Pe = [e]

    return C

  def reachAnalysisFile(self, B, C, fn, deadfn, bPrint, bComposed=False):
    """ Reachability analysis for CPN and initial tokens and dead markings
        loaded from file (e.g. produced by CPN/Tools).

        Has to allow for 2 B and C (for composed).

        File format:
          Token:a INT 3,b BOOL True,...
          DM:a=4,b=5,...;p3,p9,...  # (token;place list)
          DM:...
          ...
          Token:...
          ...
        *** Probably easer to do this in XML when we automate it.
    """
    fid = open(deadfn, 'r')
    lines = fid.readlines()
    fid.close()
    found, foundC = 0, 0  # For 2 single, and composed model

    deadassigns, printclashes = [], []

    if bPrint:
      print()
      print("Model", "Activity", "[Guard]", "Data", "(Initial)", "Type",
            "(Model)", "(Activity)", "(Var)", sep=sep)

    # Iterate over
    ll = 0
    key, data = lines[ll].split(':')
    while ll < len(lines):
      if key == "Token":
        DM = []  # Dead Markings
        vdict = {}
        for setting in data.split(','):
          var, typ, val = setting.split(' ')
          vdict[var.strip()] = {"type": typ.strip(), "value": val.strip()}
        inittoken = Token(vdict)

        ll += 1
        if ll >= len(lines):
          raise Exception("Unexpected end of dead marking file")
        key, data = lines[ll].split(':')
        while ll < len(lines) and key == "DM":
          M = []  # Marking (list of MarkedPlaces)
          varvals, mps = data.split(';')
          tokendict = {}
          for varval in varvals.split(','):
            var, val = varval.split('=')
            typ = vdict[var.strip()]["type"]
            tokendict[var.strip()] = {"type": typ.strip(), "value": val.strip()}
          token = Token(tokendict)
          for p in mps.split(','):
            p = p.strip()
            pFound = False
            # Find what the place maps to
            for place in C.P:
              if place.text == p:
                pO = place
                pFound = True
                break
            if not pFound:
              raise Exception("Place", p, "not found")
            MP = MarkedPlace(place)
            MP.produce_token(token, "dummy")
            M.append(MP)
          DM.append(Marking(M))
          ll += 1
          if ll < len(lines):
            key, data = lines[ll].split(':')

#     for M in DM: M.print()  # XXX

      if bComposed:
        deadassigns = []
        f, printclashes = \
          self.deadReportC(B, C, deadassigns, printclashes, \
            dm=DM, inittoken=inittoken)
      else:
        f, deadassigns, printclashes = \
          self.deadReport(B, C, fn, deadassigns, printclashes, \
            dm=DM, inittoken=inittoken)

      found += f
  
    # Output, ignoring duplicate (Act, init token)
    if bPrint:
      # [fn, Activity, var, guard, modvar, data-mod, token, inittoken, type,
      #  clashing Activities, clashing vars]
      for p in printclashes:
        print(p[0] + ',' + p[1].name + ','
              + '[' + p[3] + '],'
              + p[6] + ',(' + p[7] + '),'
              + p[8] + ',' + p[9] + ',' + ';'.join(set(p[10])) + ','
              + ';'.join(set(p[11])))



  def reachAnalysis(self, B, C, fn, bCompose=True, bPrint=False, log=False,
                    output_cpn=False, output_rg=False):
    """ Reachability analysis for the two CPNs and their composition.
        NB This should be broken down much further.
        B: pair of BPMN models
        C: their conversion to CPNs

        Limitations:
        o  Returns a very poor attempt at counting how many / which clashes
           have been found (assuming called from random model generator).
        o  Nominally allows list of models; actually only deals with max first
           two.
    """
    if type(B) is list:
      ll = len(B)  # Allow for use on single model or pair of models
    else:
      ll, B, C, fn = 1, [B], [C], [fn]
    runtime, runtimeC = [0] * ll, 0  # For 2 single, and composed model
    states, statesC = [0] * ll, 0  # For 2 single, and composed model
    found, foundC = [0] * ll, 0  # For 2 single, and composed model

    deadassigns, printclashes = [], []

    if bPrint:
      print()
      print("Model", "Activity", "[Guard]", "Data", "(Initial)", "Type",
            "(Model)", "(Activity)", "(Var)", sep=sep)
    for ii in range(ll):
      # Build set of tokens covering the guard options
      tokenset = C[ii].cover_tokens()
  
      # Explore reachability graphs for each data assignment
      s = sys.stdout
      tokens = range(len(tokenset))

      for tid in tokens:
        if not log:
          f = open(devnull, 'w')
          sys.stdout = f
  
        timei1 = time.time()
        rg, states = self.buildRG(C[ii], tokenset[tid], log, False)
        timeo1 = time.time()
  
        if not log:
          f.close()
          sys.stdout = s
  
        dotfile = fn[ii] + '_rg' + str(tid) + ".dot"
        pklfile = dotfile.replace(".dot", ".pkl")
        epsfile = dotfile.replace(".dot", ".eps")
  
        if output_rg:
          f = open(dotfile, 'w')
          sys.stdout = f
          rg.print(ptype="dot", indent=2)
          f.close() 
          sys.stdout = s
          cmd = "dot -Teps -o" + epsfile + ' ' + dotfile
          system(cmd)
  
        # Non-final dead markings
        timei2 = time.time()
#       f, da, pc \
        f, deadassigns, printclashes \
          = self.deadReport(B[ii], C[ii], fn[ii], deadassigns, \
              printclashes, rg=rg)
        found[ii] += f
#       deadassigns += da
#       printclashes += pc

        timeo2 = time.time()
        timeiter = (timeo2-timei2) + (timeo1-timei1)
        runtime[ii] += timeiter  # Accum. over tokens
  
    # Output, ignoring duplicate (Act, init token)
    if bPrint:
      # [fn, Activity, var, guard, modvar, data-mod, token, inittoken, type,
      #  clashing Activities, clashing vars]
#     for p in printclashes: print(p, "\n")
      for p in printclashes:
        print(p[0] + ',' + p[1].name + ','
              + '[' + p[3] + '],'
              + p[6] + ',(' + p[7] + '),'
              + p[8] + ',' + p[9] + ',' + ';'.join(set(p[10])) + ','
              + ';'.join(set(p[11])))


    ################# COMPOSED MODEL
    printclashes = []  # Accumulate clash reports, to print unique
    if bCompose:
      compos = self.compose(C[0], C[1])
  
      compfn = path.basename(fn[0]).replace(".bpmn", '') + '_' \
             + path.basename(fn[1]).replace(".bpmn", '') 
  
      # Output the composed CPN
      sout = sys.stdout
      if output_cpn:
        dotfile = compfn + "_cpn.dot"
        epsfile = dotfile.replace(".dot", ".eps")
        f = open(dotfile, 'w')
        sys.stdout = f
        compos.print(ptype="dot")
        f.close()
        sys.stdout = sout
        cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
        system(cmd)
  
      # Explore reachability graphs for each data assignment, ignoring those
      # that failed individual models.
      if bPrint:
        print()
        print("Model", "Activity", "[Guard]", "Data", "(Initial)", "Type",
              "(Model)", "(Activity)", "(Var)", sep=sep)
  
      # Set of tokens covering the guard options *** reduce token space ***
      tokenset = compos.cover_tokens()
    
      # Explore reachability graphs for each data assignment
      s = sys.stdout
      tokens = range(len(tokenset))
  
      for tid in tokens:
        # Check whether token contains data assignment that was dead for the
        # single models.  Ignore these.  This may not be what we want in the
        # long term (what if a change in model B enables A?).
        bTest = True
        ii = 0
        while ii < len(tokenset[tid].vdict.keys()) and bTest:
          var = tokenset[tid].vdict.keys()[ii]
          ass = var + '=' + str(tokenset[tid].vdict[var]["value"])
          if ass in deadassigns:
            bTest = False
          ii += 1
  
        if bTest:
          if not log:
            f = open(devnull, 'w')
            sys.stdout = f
  
          timei1 = time.time()
          rg, statesC = self.buildRG(compos, tokenset[tid], log, False)
          timeo1 = time.time()
    
          if not log:
            f.close()
            sys.stdout = s
    
          dotfile = compfn + '_rg' + str(tid) + ".dot"
          pklfile = dotfile.replace(".dot", ".pkl")
          epsfile = dotfile.replace(".dot", ".eps")
    
          if output_rg:
            f = open(dotfile, 'w')
            sys.stdout = f
            rg.print(ptype="dot", indent=2)
            f.close() 
            sys.stdout = s
            cmd = "dot -Teps -o" + epsfile + ' ' + dotfile
            system(cmd)
    
          # Non-final dead markings
          timei2 = time.time()
          f, printclashes = \
            self.deadReportC(B, compos, deadassigns, printclashes, rg=rg)
          foundC += f
    
          timeo2 = time.time()
          timeiter = (timeo2-timei2) + (timeo1-timei1)
          runtimeC += timeiter
  
    # Output, ignoring duplicate (Act, init token)
    if bPrint:
      # [fn, Activity, var, guard, modvar, data-mod, token, inittoken, type,
      #  clashing Activities, clashing vars]
      for p in printclashes:
        print(p[0] + ',' + p[1].name + ','
              + '[' + p[3] + '],'
              + p[6] + ',(' + p[7] + '),'
              + p[8] + ',' + p[9] + ',' + ';'.join(set(p[10])) + ','
              + ';'.join(set(p[11])))

    return found, foundC, runtime, runtimeC, states, statesC

  def deadReport(self, B, C, fn, deadassigns, printclashes, \
                 rg=None, dm=None, inittoken=None):
    """ Report for individual model, in reachAnalysis().
        Probably should be merged with deadReportC().
    """
    clashes = []  # Record data if clash found.
#   deadassigns = []  # Not worth checking composed model for these (???)
#   printclashes = []  # Accumulate clash reports, to print unique
    found = 0
    if rg is not None:
      deadmarkings = rg.dead
      inittoken = rg.inittoken
    else:
      deadmarkings = dm
#   for M in rg.dead:
    for M in deadmarkings:
      Pe = C.Pe[0]
      if not (len(M.MP) == 1 and M.MP[0].p == Pe):
        for mp in M.MP:
          for t in C.postset(mp.p):
            a = B.CPN_BPMN[t]
            if type(a) is Activity and not a.name.startswith("_eta"):
              if a.guard is not None:
                guards = a.guard.split(',')
  
                # Identify which variables caused the clash
                for guard in guards:
                  (var, op, val, neg) = \
                    gr.tguard.parse(guard, M.token.vdict)
                  S = neg + ' ' + str(M.token.vdict[var]["value"]) + ' ' \
                      + op + ' ' + str(val)
                  b = eval(S)
  
                  if not b:
                    clashact = a
                    clashguard = a.guard
                    clashmod = a.data
                    clashguardvar = var
  
                    clashdata = var + '=' \
                      + str(M.token.vdict[var]["value"])
                    deadassigns.append(clashdata)
  
                    clashinit = var + '=' + \
                      str(inittoken.vdict[var]["value"])
#                     str(rg.inittoken.vdict[var]["value"])
  
                    # Identify any variables modified (?)
                    clashmodvar = []
                    if a.data is not None:
                      mods = a.data.split(',')
  
                      for mod in mods:
                        (_, var, _, _, _) = \
                          gr.arcins.parse1(B.vars, mod)
                        if var not in clashmodvar:  # For this clash
                          clashmodvar.append(var)
  
                    clash = [fn, clashact, clashguardvar, clashguard,
                             clashmodvar, clashmod, clashdata, clashinit]
                    if clash not in clashes:
                      clashes.append(clash)
  
    # Accumulate clashes for output
    for jj in range(len(clashes)):
      bSelf = False
      bVars = False
  
      clash = clashes[jj]
      # If guard var is also one of the modified vars
      # (Over-prescription conflict)
      if clash[2] in clash[4]:
        # If same var occurs in the same way in guard not yet checked
        if any([a[2] == clash[2] and a[2] in a[4]
                for a in clashes[jj+1:]]):
          bSelf = True
          found += 1
          clashacts = [a[1] for a in clashes[jj+1:]
                       if a[2] == clash[2] and a[2] in a[4]]
          clashactnames = [a.name for a in clashacts]
          # Check for duplicates
          if [clash[1], clash[3], clashactnames] not in \
              [[p[1], p[3], p[10]] for p in printclashes]:
            printclashes.append(clash + ["self", clash[0], clashactnames, []])
  
        elif any([a[2] == clash[2] and a[2] in a[4]
                  for a in clashes[:jj]]):  # Check for dup. already seen
          bSelf = True
  
      # If guard var a and a modified var b
      # (Prescription conflict)
      if a not in clash[4] or len(clash[4]) > 1:
        # if another activity has guard var b and a in a mod var
        if any([a[2] != clash[2] and a[2] in clash[4] and clash[2] in a[4]
                for a in clashes[:jj] + clashes[jj+1:]]):
          bVars = True
          found += 1

          vars = [a[2] for a in clashes[:jj] + clashes[jj+1:] 
                  if a[2] in clash[4] and a[2] != clash[2]]
          clashacts = [a[1] for a in clashes[:jj] + clashes[jj+1:] 
                       if a[2] in clash[4] and a[2] != clash[2]]
          clashactnames = [a.name for a in clashacts]
          # Check for duplicates
          if [clash[1], clash[3], clashactnames, vars] not in \
              [[p[1], p[3], p[10], p[11]] for p in printclashes]:
            printclashes.append(clash + ["vars", clash[0], clashactnames, vars])
  
      # Conflict due to initial data setting
      if not (bSelf or bVars):
        # Check for duplicates
        if clash not in [p[0:8] for p in printclashes]:
          found += 1  # Maybe should not set this
          printclashes.append(clash + ["init", '', [], []])
  
    return found, deadassigns, printclashes

  def deadReportC(self, B, compos, deadassigns, printclashes, rg=None, dm=None,
      inittoken=None):
    """ Report for composed model, in reachAnalysis().
        Probably should be merged with deadReport().
    """
    clashes = []  # Record data for appropriate model if clash found.
    printclashes = []  # Accumulate clash reports, to print unique
    foundC = 0
    if rg is not None:
      deadmarkings = rg.dead
      inittoken = rg.inittoken
    else:
      deadmarkings = dm
#   for M in rg.dead:
    for M in deadmarkings:
      Pe = compos.Pe[0]
      if not (len(M.MP) == 1 and M.MP[0].p == Pe):
        for mp in M.MP:
          for t in compos.postset(mp.p):
            # Exclude these as not in the original models.
            if t.oid not in ["_eta_s_compos", "_eta_e_compos"]:
              if t.text.endswith("cpn1"):
                bpmn = 0
              elif t.text.endswith("cpn2"):
                bpmn = 1
              else:
                raise Exception("Transition name", t.text,
                                "not understood.")
              fromB = B[bpmn]
              ttext = t.text.replace("_cpn" + str(bpmn+1), '')
  
              a = B[bpmn].CPN_BPMN[t]
              if type(a) is Activity and not a.name.startswith("_eta"):
                if a.guard is not None:
                  guards = a.guard.split(',')

                  # Identify which variables caused the clash
                  for guard in guards:
                    (var, op, val, neg) = \
                      gr.tguard.parse(guard, M.token.vdict)
                    S = neg + ' ' + str(M.token.vdict[var]["value"]) \
                      + ' ' + op + ' ' + str(val)
                    b = eval(S)

                    if not b:
                      clashact = a
                      clashguard = a.guard
                      clashmod = a.data
                      clashguardvar = var

                      clashdata = var + '=' \
                        + str(M.token.vdict[var]["value"])
                      deadassigns.append(clashdata)

                      clashinit = var + '=' + \
                        str(inittoken.vdict[var]["value"])
#                       str(rg.inittoken.vdict[var]["value"])

                      # Identify any variables modified (?)
                      clashmodvar = []
                      if a.data is not None:
                        mods = a.data.split(',')

                        for mod in mods:
                          (_, var, _, _, _) = \
                            gr.arcins.parse1(B[bpmn].vars, mod)
                          if var not in clashmodvar:  # For this clash
                            clashmodvar.append(var)

                      clash = [str(bpmn),
                               clashact, clashguardvar, clashguard,
                               clashmodvar, clashmod, clashdata,
                               clashinit]
                      if clash not in clashes:
                        clashes.append(clash)
    
    # Accumulate clashes for output
    for jj in range(len(clashes)):
      bSelf = False
      bVars = False
    
      clash = clashes[jj]
      bpmn = clash[0]
      other = str((int(bpmn)+1)%2)
      # If guard var is also one of the modified vars
      # (Over-prescription conflict)
      if clash[2] in clash[4]:
        # If same var occurs in the same way other model
        if any([a[2] == clash[2] and a[2] in a[4]
                for a in clashes[jj+1:] if a[0] != bpmn]):
          bSelf = True
          foundC += 1
          clashacts = [a[1] for a in clashes[jj+1:]
                       if a[2] == clash[2] and a[2] in a[4]
                       and a[0] != bpmn]
          clashactnames = [a.name for a in clashacts]
          # Check for duplicates
          if [clash[1], clash[3], clashactnames] not in \
              [[p[1], p[3], p[10]] for p in printclashes]:
            printclashes.append(clash + ["self", other, clashactnames, []])
    
        elif any([a[2] == clash[2] and a[2] in a[4]
                  for a in clashes[:jj]]):  # Check for dup. already seen
          bSelf = True
    
      # If guard var a and a modified var b
      # (Prescription conflict)
      if a not in clash[4] or len(clash[4]) > 1:
        # if another activity has guard var b and a in a mod var
        if any([a[2] != clash[2] and a[2] in clash[4] and clash[2] in a[4]
                for a in clashes[:jj] + clashes[jj+1:]
                if a[0] != bpmn]):
          bVars = True
          foundC += 1
          vars = [a[2] for a in clashes[:jj] + clashes[jj+1:] 
                  if a[2] in clash[4] and a[2] != clash[2]
                  and a[0] != bpmn]
          clashacts = [a[1] for a in clashes[:jj] + clashes[jj+1:] 
                       if a[2] in clash[4] and a[2] != clash[2]
                       and a[0] != bpmn]
          clashactnames = [a.name for a in clashacts]
          # Check for duplicates
          if [clash[1], clash[3], clashactnames, vars] not in \
              [[p[1], p[3], p[10], p[11]] for p in printclashes]:
            printclashes.append(clash + ["vars", other, clashactnames, vars])
    
      # Conflict due to initial data setting
      if not (bSelf or bVars):
        if clash not in [p[0:8] for p in printclashes]:
          foundC += 1  # Maybe should not set this
          # Problem here, this should not (?) occur
          printclashes.append(clash + ["unk", other, [], []])

    return foundC, printclashes
