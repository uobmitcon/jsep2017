#!/usr/bin/env python2
# Phil Weber 2016-12-07 Experimental framework for BPMNV + CPN Analsyis with
# composition.
# 2016-12-15 If file of dead markings (from CPN/Tools) is supplied, load the
#            2 BPMN, do the composition (here), then pass the composed CPN for
#            analysis.  Otherwise, load and analyse both and the composed.

from __future__ import print_function
import sys
from os import path, devnull, system
import cPickle as pickle
from CPN import *
from BPMNV import *
import CPNParse
from CPNAnalysis import CPNAnalyser

sep = ','  # for output


if __name__ == "__main__":
  log = True
  timings = False
  output_bpmn = True
  output_cpn = True
  output_rg = False

  from optparse import OptionParser
  op = OptionParser()
  op.add_option('-1', '--filename1', dest="fn1",
                help="1st BPMN file", type="string", default=None)
  op.add_option('-2', '--filename2', dest="fn2",
                help="2nd BPMN file", type="string", default=None)
  op.add_option('-d', '--deadmarkings', dest="deadfn",
                help="File containing dead markings (bespoke format)",
                type="string", default="")
  op.add_option('-t', '--tokens', dest="tokens",
                help="Set if composed model.", type="int", default=None)
  opts, args = op.parse_args()

  # Tokens to apply to RG.  If not set, all in set in turn
  tokens = None
  if len(args) or opts.fn1 is None or opts.fn2 is None:
    raise Exception("Problem with parameters!")
  else:
    fn = [opts.fn1, opts.fn2]
    deadfn = None
    if opts.deadfn: deadfn = opts.deadfn
    if opts.tokens: tokens = opts.tokens

  CA = CPNAnalyser()

  # Initialise BPMN+V from file
  B = [BPMNV(fn[0]), BPMNV(fn[1])]
  sout = sys.stdout

  # Output the imported BPMNs
  if output_bpmn:
    for ii in [0, 1]:
      dotfile = path.basename(fn[ii]).replace(".bpmn", '_bpmn.dot')
      pklfile = dotfile.replace(".dot", ".pkl")
      epsfile = dotfile.replace(".dot", ".eps")
      pickle.dump(B[ii], open(pklfile, "wb"))
      f = open(dotfile, 'w')
      sys.stdout = f
      B[ii].print(ptype="dot")
      f.close()
      sys.stdout = sout
      cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
      system(cmd)

  # Convert to CPN
  C = [B[0].toCPN(), B[1].toCPN()]

  # Output the derived CPNs
  if output_cpn:
    for ii in [0, 1]:
      dotfile = path.basename(fn[ii]).replace(".bpmn", '_cpn.dot')
      pklfile = dotfile.replace(".dot", ".pkl")
      epsfile = dotfile.replace(".dot", ".eps")
      pickle.dump(C[ii], open(pklfile, "wb"))
      f = open(dotfile, 'w')
      sys.stdout = f
      C[ii].print(ptype="dot")
      f.close()
      sys.stdout = sout
      cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
      system(cmd)

  # Reachability analysis for the two CPNs and their composition.
  bPrint = True
  if deadfn:
    compos = CA.compose(C[0], C[1])

    compfn = path.basename(fn[0]).replace(".bpmn", '') + '_' \
           + path.basename(fn[1]).replace(".bpmn", '')

    # Output the composed CPN
    sout = sys.stdout
    if output_cpn:
      dotfile = compfn + "_cpn.dot"
      epsfile = dotfile.replace(".dot", ".eps")
      f = open(dotfile, 'w')
      sys.stdout = f
      compos.print(ptype="dot")
      f.close()
      sys.stdout = sout
      cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
      system(cmd)

    bComposed = True
    CA.reachAnalysisFile(
      B, compos, path.basename(compfn), deadfn, bPrint, bComposed=bComposed)
  else:
    bCompose = True
    f, fC, r, rC, s, sC = \
      CA.reachAnalysis(B, C, [path.basename(fn[0]), path.basename(fn[1])],
                       bCompose, bPrint, log, output_cpn, output_rg)
