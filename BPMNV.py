#!/usr/bin/env python2
# Phil Weber 2016-11-30

"""
Class for manipulating BPMN+V (data-enriched subset of BPMN).
- import to internal representation.
- basic output.
- translate to CPN.
- accept and visualise changes, e.g. highlight dead markings.

Limitations:
  o  Pre-process XML to remove namespaces; bodge to allow for old version of
     lxml.

Possibilities:

History:
  2016-12-01: First version; importing BPMN and visualising with dot.
  2016-12-02: Convert BPMN to CPN.
  2016-12-07: Change BPMN_CPN map to class level.  Change SequenceFlow fid to
              oid.
  2016-12-09: Fixed bug where CPN arcs were missing inscriptions.
"""

from __future__ import print_function
import sys
import random
import unicodedata, re  # for filtering control characters
from os import system
from lxml import etree
from StringIO import StringIO
from math import sqrt
from copy import copy
from CPN import *

debug = False
DIVERGING = "Diverging"
CONVERGING = "Converging"
TtoP = "TtoP"
PtoT = "PtoT"
ETA = "_eta"

# Set some defaults for dot
dg_rankdir    = "LR";      # Arcs left --> right by default
dg_ranksep    = 0.3;       # Min " between ranks
dg_fontname   = "Arial";
dg_fontsize   = 8;        # Points
dg_remincross = "true"; # dot internal
dg_margin     = "0.0.0.0";

# Edges
e_arrowsize   = 0.5;
e_fontname    = "Arial";
e_fontsize    = 8;

# Nodes
n_height      = 0.5;
n_width       = 0.5;
n_fontname    = "Arial";
n_fontsize    = 8;

control_chars = ''.join(map(unichr, range(0,32) + range(127,160)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))

### Start, End, Activity, XOR, AND, IOR nodes
class Node(object):
  pre = None  # pre-arcs
  post = None  # post-arcs
  def __init__():
    None

  def print(self):
    None

  def addPre(self, arc):
    if self.pre is None:
      self.pre = []
    self.pre.append(arc)

  def addPost(self, arc):
    if self.post is None:
      self.post = []
    self.post.append(arc)

  def rmPre(self, arc):
    if arc in self.pre:
      self.pre.remove(arc)
      if self.pre == []:
        self.pre = None
    else:
      raise Exception("Trying to remove non-existent arc from", self.name,
                      self.oid)

  def rmPost(self, arc):
    if arc in self.post:
      self.post.remove(arc)
      if self.post == []:
        self.post = None
    else:
      raise Exception("Trying to remove non-existent arc from", self.name,
                      self.oid)


### Start
class Start(Node):
  def __init__(self, sid, name):
    self.oid = sid
    self.name = "Start" if name is None or name.strip() == '' else name

  def print(self):
    name = "''" if self.name in [None, ""] else self.name.replace("\n"," ")
    print("Start: %s (%s)" % (name, self.oid))


### End
class End(Node):
  def __init__(self, eid, name):
    self.oid = eid
    self.name = "End" if name is None or name.strip() == '' else name

  def print(self):
    name = "''" if self.name in [None, ""] else self.name.replace("\n"," ")
    print("End: %s (%s)" % (name, self.oid))


### Activity
class Activity(Node):
  def __init__(self, aid, name):
    """ Define Activity attributes. """
    self.oid = aid
    self.name = "Activity" if name in [None, ""] else name
    self.guard = None  # Add later
    self.data = None  # Add later

  def print(self):
    name = "''" if self.name is None or self.name.strip() == '' \
      else self.name.replace("\n"," ")
    if self.guard is None and self.data is None:
      ann = ''
    else:
      ann = '['
      if self.guard is not None:
        ann += "guard: " + self.guard
        if self.data is not None:
          ann += ', '
      if self.data is not None:
        ann += "data: " + self.data
      ann += ']'
      
    print("Activity: %s (%s) %s" % (name, self.oid, ann))
                           
    
### XOR
class XOR(Node):
  def __init__(self, xid, name, direction, default):
    """ Define XOR Gateway attributes.
        default indicates if split or join?
    """
    self.oid = xid
    self.name = None if name is None or name.strip() == '' else name
    self.direction = direction
    self.default = default

  def print(self):
    if self.name is None:
      name = ''
    else:
      name = self.name.replace("\n"," ")
    print("XOR %s: %s (%s)" % (self.direction, name, self.oid))


### Inclusive OR
class IOR(Node):
  def __init__(self, iid, name, direction):
    """ Define IOR Gateway attributes.
        May want indication of split or join?
    """
    self.oid = iid
    self.name = None if name is None or name.strip() == '' else name
    self.direction = direction

  def print(self):
    if self.name is None:
      name = ''
    else:
      name = self.name.replace("\n"," ")
    print("IOR %s: %s (%s)" % (self.direction, name, self.oid))


### Parallel
class AND(Node):
  def __init__(self, aid, name, direction):
    """ Define AND Gateway attributes.
        May want indication of split or join?
    """
    self.oid = aid
    self.name = None if name is None or name.strip() == '' else name
    self.direction = direction

  def print(self):
    if self.name is None:
      name = ''
    else:
      name = self.name.replace("\n"," ")
    print("AND %s: %s (%s)" % (self.direction, name, self.oid))


### SequenceFlow
class SequenceFlow(object):
  def __init__(self, fid, name, fr, to):
    """ Define arc attributes.
        fr and to are P or T objects, not explicitly defined as place/trans.
        annotation (ann) is mathematical expression; may be empty.
    """
    self.oid = fid
   
    if type(name) is str: 
      name = rm_ctrl_chars(name)
    self.name = name  # unused
    self.fr = fr
    self.to = to

  def print(self):
    name = '' if self.name in [None, ""] else self.name.replace("\n"," ")
    fr = "''" if self.fr.name in [None, ""] else self.fr.name.replace("\n"," ")
    to = "''" if self.to.name in [None, ""] else self.to.name.replace("\n"," ")
    print("Sequence Flow: (%s -> %s) %s" % (fr, to, name))
  

### BPMNV
class BPMNV(object):
  validVarTypes = ["BOOL", "INT", "REAL", "STRING"]
  BPMN_CPN = {}  # Map BPMN objects to CPN
  CPN_BPMN = {}  # Map CPN objects to BPMN

  def __init__(self, fn=None):
    """ Set up empty CPN structure, optionally populating from file.
    """
    self.vars = {}  # Variable definitions {var: {type, value}} # val unused
    self.s = None  # start event
    self.e = None  # end event
    self.Act = []  # Activities
    self.XOR = []  # XOR gateways
    self.IOR = []  # IOR gateways
    self.AND = []  # Parallel gateways
    self.F = []  # Sequence Flows

    self.objMap = {}  # Map object names to object refs
    self.guards = {}  # Save annotations which are guards before add to Activity
    self.data = {}  # Save annotations which are data mods b/f add to Activity

    if fn:
      self.parse_bpmn(fn)

  def __call__(self):
    return self.print()

  def __get__(self):
    return self.print()

  def gen_rnd(self, modelsize, p_structs=None):
    """ Generate model by starting with a baseline and randomly expanding nodes.
    """
    def stroid():
      """ Generate string oids for convenience. """
      oid = 0
      while True:
        yield "rnd" + str(oid)
        oid += 1

    def expand_seq(a):
      """ Expand Activity a to sequence a-b with original start and end points.
      """
      fin = a.pre[0]  # We know there is only one
      fout = a.post[0]
      a.rmPost(fout)
      aid = next(oid)
      b = Activity(aid, aid)
      self.Act.append(b)
      f = SequenceFlow(next(oid), None, a, b)
      self.F.append(f)
      a.addPost(f)
      b.addPre(f)
      fout.fr = b  # This should be in addPost?
      b.addPost(fout)

    def expand_xor(a):
      """ Expand Activity a to XOR o-a|b-o with original start and end points.
      """
      fin = a.pre[0]  # We know there is only one
      fout = a.post[0]
      a.rmPre(fin)
      a.rmPost(fout)
      aid = next(oid)
      b = Activity(aid, aid)
      self.Act.append(b)
      xid = next(oid)
      xi = XOR(xid, xid, DIVERGING, None)
      self.XOR.append(xi)
      xid = next(oid)
      xo = XOR(xid, xid, CONVERGING, None)
      self.XOR.append(xo)
      f = SequenceFlow(next(oid), None, xi, a)  # Connect the XOR structure
      self.F.append(f)
      a.addPre(f)
      f = SequenceFlow(next(oid), None, xi, b)
      self.F.append(f)
      b.addPre(f)
      f = SequenceFlow(next(oid), None, a, xo)
      self.F.append(f)
      a.addPost(f)
      f = SequenceFlow(next(oid), None, b, xo)
      self.F.append(f)
      b.addPost(f)
      fin.to = xi
      fout.fr = xo

    def expand_ior(a):
      """ Expand Activity a to IOR o-a|b-o with original start and end points.
      """
      fin = a.pre[0]  # We know there is only one
      fout = a.post[0]
      a.rmPre(fin)
      a.rmPost(fout)
      aid = next(oid)
      b = Activity(aid, aid)
      self.Act.append(b)
      iid = next(oid)
      ii = IOR(iid, iid, DIVERGING)
      self.IOR.append(ii)
      iid = next(oid)
      io = IOR(iid, iid, CONVERGING)
      self.IOR.append(io)
      f = SequenceFlow(next(oid), None, ii, a)  # Connect the IOR structure
      self.F.append(f)
      a.addPre(f)
      f = SequenceFlow(next(oid), None, ii, b)
      self.F.append(f)
      b.addPre(f)
      f = SequenceFlow(next(oid), None, a, io)
      self.F.append(f)
      a.addPost(f)
      f = SequenceFlow(next(oid), None, b, io)
      self.F.append(f)
      b.addPost(f)
      fin.to = ii
      fout.fr = io

    def expand_and(a):
      """ Expand Activity a to AND o-a||b-o with original start and end points.
      """
      fin = a.pre[0]  # We know there is only one
      fout = a.post[0]
      a.rmPre(fin)
      a.rmPost(fout)
      aid = next(oid)
      b = Activity(aid, aid)
      self.Act.append(b)
      aid = next(oid)
      ai = AND(aid, aid, DIVERGING)
      self.AND.append(ai)
      aid = next(oid)
      ao = AND(aid, aid, CONVERGING)
      self.AND.append(ao)
      f = SequenceFlow(next(oid), None, ai, a)  # Connect the AND structure
      self.F.append(f)
      a.addPre(f)
      f = SequenceFlow(next(oid), None, ai, b)
      self.F.append(f)
      b.addPre(f)
      f = SequenceFlow(next(oid), None, a, ao)
      self.F.append(f)
      a.addPost(f)
      f = SequenceFlow(next(oid), None, b, ao)
      self.F.append(f)
      b.addPost(f)
      fin.to = ai
      fout.fr = ao

    # Counters
    (seq, xor, ior, pll) = (0, 0, 0, 0)

    # Roulette probabilities for expanding nodes
    # (IOR issues with creating guards that cannot be well detected.)
    if p_structs:  # previously confirmed summation to 1.0
      p_seq, p_xor, p_ior, p_and = p_structs
    else:
      p_seq = 0.6 # 0.5
      p_xor = 0.1
      p_ior = 0.0 # 0.15
      p_and = 0.3 # 0.15
    p = [p_seq, p_xor, p_ior, p_and]
    [p_seq, p_xor, p_ior, p_and] = [p[i] + sum(p[0:i]) for i in range(len(p))]

    # Initialise base model o-A-o
    oid = stroid()
    name = ""
    self.s = Start(next(oid), name)
    self.e = End(next(oid), name)
    aid = next(oid)
    a = Activity(aid, aid)
    self.Act.append(a)
    f1 = SequenceFlow(next(oid), None, self.s, a)
    f2 = SequenceFlow(next(oid), None, a, self.e)
    a.addPre(f1)
    a.addPost(f2)
    self.F.append(f1)
    self.F.append(f2)

    # Expand random Activities.
    import random, time
    random.seed(time.time())

    for iters in range(1,modelsize):
      ii = random.randint(0, len(self.Act)-1)  # which Activity to expand
      a = self.Act[ii]
      p = random.random()  # dice roll
      if p < p_seq:
        expand_seq(a)
        seq += 1
      elif p < p_xor:
        expand_xor(a)
        xor += 1
      elif p < p_ior:
        expand_ior(a)
        ior += 1
      elif p < p_and:
        expand_and(a)
        pll += 1

    return seq, xor, ior, pll

  def print(self, ptype=None):
    """ Pretty output in internal representation format, GraphViz dot or
        CPN/Tools XML - buggy and at present no layout (perhaps could render
        a layout e.g. using GraphViz dot to xdot format, then read that),
        or colour set and variable etc. definitions.
    """
    if ptype not in ["dot"]:
      for v in self.vars: print("var:", v, ':', self.vars[v]["type"],
                                self.vars[v]["value"])
      self.s.print()
      self.e.print()
      for a in self.Act: a.print()
      for x in self.XOR: x.print()
      for i in self.IOR: i.print()
      for a in self.AND: a.print()
      for f in self.F: f.print()

    else:
      # Output graph header, defaults etc.
      print('digraph BPMN_V {')
      print('  forcelabels=true;')
      print('  ranksep="%s"; fontsize="%s"; remincross=%s; '
            % (dg_ranksep, dg_fontsize, dg_remincross))
      print('margin="%s"; fontname="%s";rankdir="%s";'
            % (dg_margin, dg_fontname, dg_rankdir))

      print('  edge [arrowsize="%s", fontname="%s",fontsize="%s"];' %
        (e_arrowsize, e_fontname, e_fontsize))

      print('  node [height="%s",width="%s",fontname="%s",'
            % (n_height, n_width, n_fontname))
      print('fontsize="%s"];'
            % (n_fontsize))

      # Start and End nodes
      for o in [self.s]:
        label = ''
        xlabel = None if o.name in [None, ""] else o.name.replace("\n","\\n")
        xlabel = wrap(xlabel)
        if len(self.vars.keys()) >= 1:
          bFirst = True
          for var in self.vars.keys():
            xlabel += "\\n"
            if bFirst:
              xlabel += '('
              bFirst = False
            xlabel += self.vars[var]["type"] + ": " + var
          xlabel += ')'
        if xlabel is None:
          print('  %s [shape="circle",label="%s",height=%s,width=%s' % 
            (o.oid, label, 0.3, 0.3), end='')
        else:
          print(
            '  %s [shape="circle",label="%s",xlabel="%s",height=%s,width=%s' % 
            (o.oid, label, xlabel, 0.3, 0.3), end='')
        print("];")

      for o in [self.e]:
        label = ''
        xlabel = None if o.name in [None, ""] else o.name.replace("\n","\\n")
        xlabel = wrap(xlabel)
        if xlabel is None:
          print('  %s [shape="circle",label="%s",height=%s,width=%s' % 
            (o.oid, label, 0.3, 0.3), end='')
        else:
          print(
            '  %s [shape="circle",label="%s",xlabel="%s",height=%s,width=%s' % 
            (o.oid, label, xlabel, 0.3, 0.3), end='')
        print(",penwidth=2];")

      # Activities.
      for a in self.Act:
        label = a.name.replace('\n','\\n')
        xlabel = None
        if a.guard is not None:
          # label = label + "\\n[" + a.guard + ']'
          xlabel = '[' + a.guard + ']'
        label = wrap(label)
        if a.data is not None:
          if xlabel is None:
            xlabel = ''
          xlabel += "[" + a.data + ']'
        if xlabel is None:
          print('  %s [shape="box",label="%s"' % 
            (a.oid, label), end='')
        else:
          xlabel = wrap(xlabel)
          print('  %s [shape="box",label="%s",xlabel="%s"' % 
            (a.oid, label, xlabel), end='')
        if label.startswith(ETA):
          print("style=filled,fillcolor=black,fontcolor=white,width=0.1")
        print("];")
  
      # Gateways.
      for g in self.XOR + self.IOR + self.AND:
        label = 'X' if type(g) is XOR else 'O' if type(g) is IOR else '+'
        xlabel = None
        if g.name is not None:
          xlabel = wrap(g.name.replace("\n","\\n"))
          print(
            '  %s [shape="diamond",label="%s",xlabel="%s",width=%s,height=%s' % 
            (g.oid, label, xlabel, 0.3, 0.3), end='')
        else:
          print('  %s [shape="diamond",label="%s",width=%s,height=%s' % 
            (g.oid, label, 0.3, 0.3), end='')
        if xlabel is None:
          print(",fontsize=16];")
        else:
          print("];")
  
      # Sequence Flows (arcs)
      for f in self.F:
        penwidth=1
        if type(f.fr) is XOR:
          if f.fr.default == f:
            penwidth=3
        if f.name is not None:
          print('  %s -> %s [label="%s",penwidth=%i];' \
              % (f.fr.oid, f.to.oid, f.name, penwidth))
        else:
          print('  %s -> %s [penwidth=%i];' % (f.fr.oid, f.to.oid, penwidth))

      # Print graph trailer.
      print("}")


  def parse_bpmn(self, fn):
    # ASSUMPTION:  Variable:type list as annotation to Start node;
    #              type in ["INT", "REAL", "BOOL", "STRING"].
    #              Optional text annotations to activity "guard:" or "data:"
    #              to specify transition guard or data modification.

    """ Import BPMN: currently assumes BPMN.io format (if it matters).
        Pre-process XML to string and basic removal of namespace information,
        to allow for older versions of LXML.
    """
    try: 
      s = ""
      f = open(fn, 'r')
      for line in f:
        s += line.replace("bpmn2:", "").replace("bpmn:", "")
      f.close()
    except IOError:
      raise SystemError("Problem opening", fn, "!")
    tree = etree.parse(StringIO(s))

    root = tree.getroot()
    #   Removed: for old version of LXML
    #   try:  # Need to look into this. BPMN.io vs Eclipse modelling?
    #     nsbpmn = root.nsmap["bpmn"]
    #   except:
    #     try: nsbpmn = root.nsmap["bpmn2"]
    #     except:
    #       raise Exception("Failed to find nsmap!")
    #   ns = {"ns": nsbpmn}

    B = root.find("process")

    # Start node
    #S = root.find("ns:process", ns).find("ns:startEvent", ns)
    #S = root.find('{' + nsbpmn + "}process").find('{' + nsbpmn + "}startEvent")
    S = B.find("startEvent")
    sid = S.attrib["id"]
    try:
      name = S.attrib["name"]
    except:
      name = ''
    self.s = Start(sid, name)
    self.objMap[sid] = self.s

    # End node
    E = B.find("endEvent")
    eid = E.attrib["id"]
    try:
      name = E.attrib["name"]
    except:
      name = ''
    self.e = End(eid, name)
    self.objMap[eid] = self.e

    # Activities
    A = B.findall("task")
    for a in A:
      aid = a.attrib["id"]
      name = a.attrib["name"]
      O = Activity(aid, name)
      self.Act.append(O)
      self.objMap[aid] = O

    # XOR splits (exclusive OR)
    X = B.findall("exclusiveGateway")
    for x in X:
      xid = x.attrib["id"]
      try:
        name = x.attrib["name"]
      except:
        name = None
      direction = self.getdirection(x)
      try:
        default = x.attrib["default"]  # (replaced later with SequenceFlow)
      except:
        default = None
      O = XOR(xid, name, direction, default)
      self.XOR.append(O)
      self.objMap[xid] = O

    # IOR splits (inclusive OR)
    I = B.findall("inclusiveGateway")
    for i in I:
      iid = i.attrib["id"]
      try:
        name = i.attrib["name"]
      except:
        name = None
      direction = self.getdirection(i)
      # Assuming no default
      O = IOR(iid, name, direction)
      self.IOR.append(O)
      self.objMap[iid] = O

    # AND splits (parallel)
    A = B.findall("parallelGateway")
    for a in A:
      aid = a.attrib["id"]
      try:
        name = a.attrib["name"]
      except:
        name = None
      direction = self.getdirection(a)
      # Assuming no default
      O = AND(aid, name, direction)
      self.AND.append(O)
      self.objMap[aid] = O

    # Sequence Flows: must be imported after node objects
    F = B.findall("sequenceFlow")
    for f in F:
      fid = f.attrib["id"]
      try:
        name = f.attrib["name"]
      except:
        name = None
      fr = self.objMap[f.attrib["sourceRef"]]
      to = self.objMap[f.attrib["targetRef"]]
      O = SequenceFlow(fid, name, fr, to)
      self.F.append(O)
      self.objMap[fid] = O

    # Replace XOR default flow names with Sequence Flow objects
    for x in self.XOR:
      if x.default is not None:
        x.default = self.objMap[x.default]

    # Annotations:
    #   text beginning: "guard:" for activity guard.
    #                   "data:" for data modification statement on activity.
    #                   "int|real|string|bool:" for var def (on start event).
    # 1. Find and save annotations.
    A = B.findall("textAnnotation")
    for a in A:
      aid = a.attrib["id"]
      text = a.find("text").text
      text = text.split(':')
      if len(text) > 1:
        anntype = text[0]  # expect "guard:" etc.
        anntext = ':'.join(text[1:]).strip()  # allow for ':' in remainder
        if anntype == "guard":
          self.guards[aid] = anntext
        elif anntype == "data":
          self.data[aid] = anntext
        elif anntype.upper() in self.validVarTypes:
          ann = anntype + ':' + anntext  # allow for list of vars
          vardefs = ann.split(',')
          for vardef in vardefs:
            [anntype, anntext] = vardef.split(':')
            self.vars[anntext.strip()] = {"type": anntype.strip().upper(),
                                  "value": None}
        else:
          print("Ignoring invalid annotation", anntype + ": " + anntext)

    # 2. Find associations and use to allocate annotations to correct objects.
    A = B.findall("association")
    for a in A:
      fr = a.attrib["sourceRef"]
      to = a.attrib["targetRef"]
      if to in self.guards.keys():
        if fr in self.objMap and type(self.objMap[fr]) is Activity:
          if self.objMap[fr].guard is None:
            self.objMap[fr].guard = self.guards[to]
          else:
            print(fr, "already has a guard!")
        else:
          print("Ignoring invalid association!", self.guards[to])
      elif to in self.data.keys():
        if fr in self.objMap and type(self.objMap[fr]) is Activity:
          if self.objMap[fr].data is None:
            self.objMap[fr].data = self.data[to]
          else:
            print(fr, "already has a data annotation!")
        else:
          print("Ignoring invalid association!", self.data[to])

    # Variables: currently to be defined in annotation to start node

  def getdirection(self, gw):
    try:
      direction = gw.attrib["gatewayDirection"]
    except:
      I = gw.findall("incoming")
      O = gw.findall("outgoing")
      if len(I) == 1 and len(O) > 1:
        direction = DIVERGING
      elif len(O) == 1 and len(I) > 1:
        direction = CONVERGING
      else:
        direction = None
    return direction

  def toCPN(self):
    """ Convert (restricted) BPMN+V to (restricted) CPN. """
    self.BPMN_CPN = {}  # Keep track of mapped objects
    oid = genoid()  # For CPN objects which don't directly map to BPMN objects

    # Tell each node about its incoming (pre) and outgoing arcs (post)
    for f in self.F:
      f.fr.addPost(f)
      f.to.addPre(f)

    cpn = CPN()
    datatype = None  # Not using place datatypes (CPN/Tools concept).

    # VARIABLES
    cpn.CPNvars = self.vars

    # START event: Map node to start place (plus arc)
    p = cpn.addPlace(self.s.oid, self.s.name, datatype)
    self.BPMN_CPN[self.s] = p

    # END event: Map node to (plus arc) end place
    p = cpn.addPlace(self.e.oid, self.e.name, datatype)
    self.BPMN_CPN[self.e] = p

    # Add dummy activity between all pairs of contiguous gateways, to ensure
    # the final CPN is not fragmented.  Redundant etas will be removed later.
    for o in self.XOR + self.IOR + self.AND:
      if len(o.pre) == 1: # and len(o.pre[0].fr.post) == 1:
#       if o.pre[0].fr in [self.XOR + self.IOR + self.AND]:
          tid = next(oid)
          O = self.insertEtaAct(oid, o.pre[0], tid)
      if len(o.post) == 1: # and len(o.post[0].fr.pre) == 1:
#       if o.post[0].to in [self.XOR + self.IOR + self.AND]:
          tid = next(oid)
          O = self.insertEtaAct(oid, o.post[0], tid)

    # ACTIVITIES map to Transition (with arcs)
    for a in self.Act:
      t = cpn.addTrans(a.oid, a.name, a.guard)
      self.BPMN_CPN[a] = t

    # GATEWAYS: Add, recording which SequenceFlows are mapped.

    # Map Diverging and corresponding Converging IOR Gateways together to
    # diverging/converging combination of XOR and AND.
    for i in self.IOR:
      if i.direction == DIVERGING:
        self.remapIOR(oid, i)  # Also remaps the corresponding closing IOR

    # AND (PARALLEL): map previous/following Activity to diverging/converging
    # transition plus places
    # AND DIVERGING
    for a in self.AND + self.IOR:  # IORs have been changed
      if a.direction == DIVERGING:
        if len(a.pre) > 1:
          raise Exception("Invalid AND diverging gateway with", len(a.pre),
                          "inflows.")
        iflow = a.pre[0]
        oflows = a.post
        pre = iflow.fr
        self.BPMN_CPN[iflow] = None  # This arc is subsumed
        if type(pre) is Activity:
          preact = self.BPMN_CPN[pre]
        else:
          # Add eta
          preact = self.addetaNoArc(cpn, oid)

        self.BPMN_CPN[a] = preact  # Save mapped start of AND

        for oflow in oflows:
          post = oflow.to
          # Add a place and a link to the next Activity.
          pid = next(oid)
          postplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          aid = next(oid)
          f = cpn.addArc("ID" + str(aid), TtoP, preact.oid, postplace.oid)
          if type(post) is Activity:
            aid = next(oid)
            f = cpn.addArc("ID" + str(aid), PtoT, post.oid, postplace.oid)
            self.BPMN_CPN[oflow] = f
          else:
            eta = self.addeta(cpn, oid, PtoT, postplace, oflow, True,
                              "fr")
      else:
        None

    # AND CONVERGING
    for a in self.AND + self.IOR:  # IORs have been changed
      if a.direction == CONVERGING:
        if len(a.post) > 1:
          raise Exception("Invalid AND converging gateway with", len(a.post),
                          "outflows.")
        oflow = a.post[0]
        iflows = a.pre
        post = oflow.to
        self.BPMN_CPN[oflow] = None  # This arc is subsumed
        if type(post) is Activity:
          postact = self.BPMN_CPN[post]
        else:
          # Add eta
          postact = self.addetaNoArc(cpn, oid)

        self.BPMN_CPN[a] = postact  # Save mapped end of AND

        for iflow in iflows:
          pre = iflow.fr
          # Add a place and a link to the next Activity.
          pid = next(oid)
          preplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          aid = next(oid)
          f = cpn.addArc("ID" + str(aid), PtoT, postact.oid, preplace.oid)
          if type(pre) is Activity:
            aid = next(oid)
            ann = pre.data
            f = cpn.addArc("ID" + str(aid), TtoP, pre.oid, preplace.oid, ann)
            self.BPMN_CPN[iflow] = f
          else:
            eta = self.addeta(cpn, oid, TtoP, preplace, iflow, True,
                              "to")
      else:
        None

    # XOR DIVERGING: map to diverging place plus transitions
    for x in self.XOR:
      if x.direction == DIVERGING:
        if len(x.pre) > 1:
          raise Exception("Invalid XOR diverging gateway with", len(x.pre),
                          "inflows.")
        iflow = x.pre[0]
        oflows = x.post
        pre = iflow.fr
        if pre is self.s:
          # This seems odd but is not a problem
          preplace = self.BPMN_CPN[self.s]
        else:
          # Do we need an eta?  Maybe not.
          pid = next(oid)
          preplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          if type(pre) is Activity:
            # Add in the arc from the previous Activity
            aid = next(oid)
            ann = pre.data
            f = cpn.addArc("ID" + str(aid), TtoP, pre.oid, preplace.oid, ann)
            self.BPMN_CPN[iflow] = f
          else:
            # Assume gateway.  Add a local dummy (eta) Activity to smooth later
            # processing of the preceding gateway.
            eta = self.addeta(cpn, oid, TtoP, preplace, iflow, True,
                              "to")
        self.BPMN_CPN[x] = preplace  # Save mapped start of XOR

        for oflow in oflows:
          post = oflow.to
          aid = next(oid)
          if type(post) is Activity:
            ann = None  # No inscription on arcs leading to Transitions
            f = cpn.addArc("ID" + str(aid), PtoT, \
              self.BPMN_CPN[post].oid, preplace.oid, ann)
            self.BPMN_CPN[oflow] = f
          else:
            # Assume that the diverging gateway is followed directly by a
            # converging gateway, i.e. this arc bypasses the rest of the XOR.
            # Model with an eta-transition on the CPN path.  Also add a dummy
            # (eta) transition to the BPMN to facilitate connecting the 
            # converging XOR gateway.
            posteta = self.addeta(cpn, oid, PtoT, preplace, oflow,
                                  True, "fr")
      else:
        None            

    # XOR CONVERGING gateways (map separately to account correctly for inserted
    # eta transitions).
    for x in self.XOR:
      if x.direction == CONVERGING:
        if len(x.post) > 1:
          raise Exception("Invalid XOR converging gateway with", len(x.post),
                          "outflows.")
        oflow = x.post[0]
        iflows = x.pre
        post = oflow.to
        if post is self.e:
          # This seems odd but is not a problem
          postplace = self.BPMN_CPN[self.e]
        else:
          # Do we need an eta?  Maybe not.
          pid = next(oid)
          postplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          if type(post) is Activity:
            # Add in the arc to the following Activity
            aid = next(oid)
            ann = None  # No inscriptions on arcs to transitions
            f = cpn.addArc("ID" + str(aid), PtoT, post.oid, postplace.oid, ann)
            self.BPMN_CPN[oflow] = f
          else:
            # Assume gateway.  Add a local dummy (eta) Activity to smooth later
            # processing of the preceding gateway.
            eta = self.addeta(cpn, oid, PtoT, postplace, oflow, True,
                              "fr")
        self.BPMN_CPN[x] = postplace  # Save mapped start of XOR

        for iflow in iflows:
          pre = iflow.fr
          aid = next(oid)
          if type(pre) is Activity:
            ann = pre.data
            f = cpn.addArc("ID" + str(aid), TtoP, \
              self.BPMN_CPN[pre].oid, postplace.oid, ann)
            self.BPMN_CPN[iflow] = f
          else:
            # Assume that the converging gateway is preceded directly by a
            # diverging gateway, i.e. this arc bypasses the rest of the XOR.
            # Model with an eta-transition on the CPN path.  Also add a dummy
            # (eta) transition to the BPMN to facilitate connecting the 
            # diverging XOR gateway.
            preeta = self.addeta(cpn, oid, TtoP, postplace, iflow,
                                  True, "to")
      else:
        None

    for g in self.XOR + self.IOR + self.AND:
      if g.direction not in [CONVERGING, DIVERGING]:
        raise Exception("Did not understand Gateway direction type",
                        g.direction)

    # Add remaining Sequence Flows.
    for f in self.F:
      if f not in self.BPMN_CPN.keys():
        # Assume nodes will have been mapped.
        # Determine what the actual end points should be, P/T, and direction.
        aid = next(oid)
        fr = self.BPMN_CPN[f.fr]
        to = self.BPMN_CPN[f.to]
        if f.fr == self.s:
          ann = None
          f = cpn.addArc("ID" + str(aid), PtoT, to.oid, fr.oid, ann)
        elif f.to == self.e:
          if type(f.fr) is Activity:
            ann = f.fr.data
          else:
            ann = None
          f = cpn.addArc("ID" + str(aid), TtoP, fr.oid, to.oid, ann)
        elif type(f.fr) is Activity and type(f.to) is Activity:
          pid = next(oid)
          midplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          ann = f.fr.data
          f = cpn.addArc("ID" + str(aid), TtoP, fr.oid, midplace.oid, ann)
          aid = next(oid)
          ann = None
          f = cpn.addArc("ID" + str(aid), PtoT, to.oid, midplace.oid, ann)
        elif type(f.fr) is Activity and type(f.to) is XOR:
          ann = f.fr.data
          f = cpn.addArc("ID" + str(aid), TtoP, fr.oid, to.oid, ann)
        elif type(f.fr) is XOR and type(f.to) is Activity:
          ann = None
          f = cpn.addArc("ID" + str(aid), PtoT, to.oid, fr.oid, ann)
        elif type(f.fr) is Activity and type(f.to) is AND:
          pid = next(oid)
          midplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          ann = f.fr.data
          f = cpn.addArc("ID" + str(aid), TtoP, fr.oid, midplace.oid, ann)
          aid = next(oid)
          ann = None
          f = cpn.addArc("ID" + str(aid), PtoT, to.oid, midplace.oid, ann)
        elif type(f.fr) is AND and type(f.to) is Activity:
          pid = next(oid)
          midplace = cpn.addPlace("ID" + str(pid), 'p' + str(pid), datatype)
          ann = None
          f = cpn.addArc("ID" + str(aid), TtoP, fr.oid, midplace.oid, ann)
          aid = next(oid)
          ann = None
          f = cpn.addArc("ID" + str(aid), PtoT, to.oid, midplace.oid, ann)
        else:
          print(type(f.fr), f.fr.name, type(f.to), f.to.name)
          raise Exception("Unexpected arc to add.")

    # Remove useless eta-transitions
    removed = float('inf')
    while removed != 0:
      removed = cpn.rm_superfluous_eta()

    # Set start and end places.
    cpn.start_places()
    cpn.end_places()

    # Convert map from CPN to BPMN objects
    self.bpmn_cpn_oids = {}
    self.cpn_bpmn_oids = {}
    for bpmn in self.BPMN_CPN.keys():
#     print("Mapping", self.BPMN_CPN[bpmn], "to", bpmn)
      self.CPN_BPMN[self.BPMN_CPN[bpmn]] = bpmn
      if bpmn is not None and self.BPMN_CPN[bpmn] is not None:
        self.bpmn_cpn_oids[bpmn.oid] = self.BPMN_CPN[bpmn].oid
        self.cpn_bpmn_oids[self.BPMN_CPN[bpmn].oid] = bpmn.oid

    return cpn

  def remapIOR(self, oid, ior):
    """ Remap a diverging IOR gateway G and its corresponding converging
        gateway G':
         o  Replace G with diverging AND gateway and XOR on each outflow.
         o  Parse from G to find G' starting from first outflow of G.  (Simply 
            take one outflow from each successive node until G' found.  Account
            for nested IORs).
         o  Replace G' similarly.
         o  Create a `bypass' arc between the pair of XOR nodes.
         o  Repeat creation of XOR pair and bypass arc for each outflow from G.
         o  Error if all outflows do not end up at the same converging IOR.
        Assumes well-structured workflow.
    """
    nested = 0  # Keep track of (ignore) nested IOR blocks.
    ior_ = None  # Found converging IOR

    toproc = []
    for f in ior.post:
      node = f.to  # parse to a succeeding node
      ii = 0
      while not (type(node) is IOR and node.direction == CONVERGING \
                 and nested <= 0):
        if type(node) is IOR:
          if node.direction == DIVERGING:  # Ignore nested IORs
            nested += 1
          elif node.direction == CONVERGING and nested > 0:
            nested -= 1
        f_ = node.post[0]  # select the first outflow arc
        node = f_.to  # parse to a succeeding node

      if ior_ is None:
        ior_ = node  # matching converging IOR gateway
      elif node != ior_:
        raise Exception("IOR diverging gateway led to multiple converging!")

      # Insert XORs after ior and before ior_
      direction = DIVERGING
      (xordiv, f1, f2) = self.insertXOR(oid, f, direction)

      direction = CONVERGING
      (xorconv, f_1, f_2) = self.insertXOR(oid, f_, direction)

      # Add arc to bypass the execution flow on this path
      arc = ''
      fid = next(oid) 
      bypass = SequenceFlow(fid, arc, xordiv, xorconv)
      self.F.append(bypass)
      bypass.fr.addPost(bypass)
      bypass.to.addPre(bypass)

    # If retaining the modified model, we would want to change the IOR
    # gateways to AND now.
    return None
        
  def insertXOR(self, oid, sf, direction):
    """ Split Sequence Flow sf using an XOR.
        Used when converting IOR to AND+XOR.
    """
    xid = "XOR" + str(next(oid))
    default = None
    name = ''
    xor = XOR(xid, name, direction, default)
    self.XOR.append(xor)
    self.objMap[xid] = xor

    (f, f_) = self.splitArc(oid, sf, xor)
    return (xor, f, f_)

  def splitArc(self, oid, sf, node):
    fr = sf.fr
    to = sf.to
    to.rmPre(sf)
    sf.to = node
    sf.to.addPre(sf)
    fid = next(oid) 
    arc = ''
    f_ = SequenceFlow(fid, arc, node, to)
    self.F.append(f_)
    f_.fr.addPost(f_)
    f_.to.addPre(f_)

    return (sf, f_)

  def splitArc1(self, oid, sf, node):
    fr = sf.fr
    to = sf.to
    fr.rmPost(sf)
    sf.fr = node
    sf.fr.addPost(sf)
    fid = next(oid) 
    arc = ''
    f_ = SequenceFlow(fid, arc, fr, node)
    self.F.append(f_)
    f_.fr.addPost(f_)
    f_.to.addPre(f_)

    return (sf, f_)

  def addeta(self, cpn, oid, direction, place, arc,
             bAddDummyAct=False, keep="fr"):
    """ Add a null (eta) transition to the CPN and corresponding arc.  If
        necessary, add a corresponding dummy BPMN Activity.
        "keep" indicates which way to split the arc.
        "to" indicates keep incoming end.
        "fr" indicates keep outgoing end.
    """
    tid = next(oid)
    guard = None
    eta = cpn.addTrans("ID" + str(tid), ETA + str(tid), guard)
    aid = next(oid)
    ann = None
    f = cpn.addArc("ID" + str(aid), direction, eta.oid, place.oid, ann)
    self.BPMN_CPN[arc] = f

    if bAddDummyAct:
      # Insert corresponding eta-Activity into the BPMN+V arc so the
      # correct connections will be made when parsing the corresponding
      # converging gateway.
      O = self.insertEtaAct(oid, arc, tid, keep)
      self.BPMN_CPN[O] = eta
      self.BPMN_CPN[O.pre[0]] = None  # Record that these arcs have no mapping
      self.BPMN_CPN[O.post[0]] = None  # to the CPN

    return eta

  def addetaNoArc(self, cpn, oid):
    """ Add a null (eta) transition to the CPN. """
    tid = next(oid)
    guard = None
    eta = cpn.addTrans("ID" + str(tid), ETA + str(tid), guard)

    return eta


  def insertEtaAct(self, oid, sf, tid, keep="fr"):
    """ Split Sequence Flow sf using the Activity act.
        Used when creating CPN eta-transitions, to avoid duplication.
    """
    O = Activity("ID" + str(tid), ETA + str(tid))
    self.Act.append(O)
#   print("Inserting ETA between", sf.fr, "(", sf.fr.name, ") and", sf.to, "(", sf.to.name, ")")

    self.objMap[tid] = O
    if keep == "fr":
      self.splitArc(oid, sf, O)
    else:
      self.splitArc1(oid, sf, O)

    return O


# Class-level functions
def rnd_conflict(B, conflicts, p_conflicts=None):
  """ Add random guard/data modification conflict between two BPMN+V models.
      B: list of 2 BPMN+V models.
      conflicts: how many conflicts to add.
  """
  chri = 97  # Variable names from 'a'

  if len(B) != 2:
    raise Exception("rnd_conflict requires a list of 2 BPMNV+V models.")

  cfs = [0,0,0,0,0,0]  # a:a (mod1,2), a:b (mod1,2), 
  cf1v1m = [0,0]
  cf2v1m = [0,0]
  cf1v2m = 0
  cf2v2m = 0

  # Add conflicts that cause the models to block each other, for simplicity of
  # analysis.  We may want to relax this in the future.
  for ii in range(conflicts):
    # 2 types of conflict (may overlap, in one or two models):
    # 1.  Mimic conflicting drug prescriptions:
    #     Model A: a=false => b=true | a<1 => b+1
    #     Model B: b=false => a=true | b<1 => a+1
    # 2.  Mimic over-prescription:
    #     Model A: a=false => a=true | a<1 => a+1
    #     Model B: ------------- ditto ----------

    # Allow: 1. over-prescription conflicts (in one or between models)
    # i.e. two activities have both [a>1][a+1]
    #        2. drug-drug conflicts (in one or between models)
    # i.e. two activities have [a>1][b+1] and [b>1][a+1]

    if p_conflicts:
      p_1v_1m, p_2v_1m, p_1v_2m, p_2v_2m = p_conflicts
    else:
      if len(B[0].Act) > 1 and len(B[1].Act) > 1:
        p_1v_1m = 0.1  # Roulette for adding various conflicts
        p_2v_1m = 0.1
        p_1v_2m = 0.4
        p_2v_2m = 0.4
      else:
        p_1v_1m = 0
        p_2v_1m = 0
        p_1v_2m = 0.5
        p_2v_2m = 0.5
    p = [p_1v_1m, p_2v_1m, p_1v_2m, p_2v_2m]
    [p_1v_1m, p_2v_1m, p_1v_2m, p_2v_2m] = \
      [p[i] + sum(p[0:i]) for i in range(len(p))]
    p = random.random()

    # Single model
    # 1. over-prescription
    if p < p_1v_1m:
      ii = random.randint(0,1)
      a1i = random.randint(0, len(B[ii].Act)-1)  # Activity 1
      a2i = copy(a1i)
      while a2i == a1i:
        a2i = random.randint(0, len(B[ii].Act)-1)  # Activity 2

      a1 = B[ii].Act[a1i]
      a2 = B[ii].Act[a2i]
      if a1.guard is not None: a1.guard += ','
      else: a1.guard = ''
      if a1.data is not None: a1.data += ','
      else: a1.data = ''
      if a2.guard is not None: a2.guard += ','
      else: a2.guard = ''
      if a2.data is not None: a2.data += ','
      else: a2.data = ''
      a1.guard += chr(chri) + "<1"
      a2.guard += chr(chri) + "<1"
      a1.data += chr(chri) + "+1"
      a2.data += chr(chri) + "+1"
      B[ii].vars[chr(chri)] = {"type": "INT", "value": None}
      cf1v1m[ii] += 1

      # 2. drug-drug
    elif p < p_2v_1m:
      ii = random.randint(0,1)
      a1i = random.randint(0, len(B[0].Act)-1)  # Guard/data Act 1
      a2i = copy(a1i)
      while a2i == a1i:
        a2i = random.randint(0, len(B[1].Act)-1)  # Guard/data Act 2

      a1 = B[ii].Act[a1i]
      a2 = B[ii].Act[a2i]
      chrj = copy(chri)  # Second variable
      chri += 1
      if a1.guard is not None: a1.guard += ','
      else: a1.guard = ''
      if a1.data is not None: a1.data += ','
      else: a1.data = ''
      if a2.guard is not None: a2.guard += ','
      else: a2.guard = ''
      if a2.data is not None: a2.data += ','
      else: a2.data = ''
      a1.guard += chr(chri) + "<1"
      a2.guard += chr(chrj) + "<1"
      a2.data += chr(chri) + "+1"
      a1.data += chr(chrj) + "+1"
      B[ii].vars[chr(chri)] = {"type": "INT", "value": None}
      B[ii].vars[chr(chrj)] = {"type": "INT", "value": None}
      cf2v1m[ii] += 1

    # Two models
    # Single variable, equivalent guard/mod in each model, on same activity.
    elif p < p_1v_2m:
      a1i = random.randint(0, len(B[0].Act)-1)  # Guard/Data Act in Model A
      a2i = random.randint(0, len(B[1].Act)-1)  # Guard/Data Act in Model B
      a1 = B[0].Act[a1i]
      a2 = B[1].Act[a2i]
      if a1.guard is not None: a1.guard += ','
      else: a1.guard = ''
      if a1.data is not None: a1.data += ','
      else: a1.data = ''
      if a2.guard is not None: a2.guard += ','
      else: a2.guard = ''
      if a2.data is not None: a2.data += ','
      else: a2.data = ''
      a1.guard += chr(chri) + "<1"
      a2.guard += chr(chri) + "<1"
      a1.data += chr(chri) + "+1"
      a2.data += chr(chri) + "+1"
      B[0].vars[chr(chri)] = {"type": "INT", "value": None}
      B[1].vars[chr(chri)] = {"type": "INT", "value": None}
      cf1v2m += 1

    # More than one variable, guard/data on one act, data/guard on other
    else:
      a1i = random.randint(0, len(B[0].Act)-1)  # Guard/Data Act in Model A
      a2i = random.randint(0, len(B[1].Act)-1)  # Data/Guard Act in Model B
      a1 = B[0].Act[a1i]
      a2 = B[1].Act[a2i]
      chrj = copy(chri)  # Second variable
      chri += 1
      if a1.guard is not None: a1.guard += ','
      else: a1.guard = ''
      if a1.data is not None: a1.data += ','
      else: a1.data = ''
      if a2.guard is not None: a2.guard += ','
      else: a2.guard = ''
      if a2.data is not None: a2.data += ','
      else: a2.data = ''
      a1.guard += chr(chri) + "<1"
      a2.guard += chr(chrj) + "<1"
      a1.data += chr(chrj) + "+1"
      a2.data += chr(chri) + "+1"
      B[0].vars[chr(chri)] = {"type": "INT", "value": None}
      B[1].vars[chr(chri)] = {"type": "INT", "value": None}
      B[0].vars[chr(chrj)] = {"type": "INT", "value": None}
      B[1].vars[chr(chrj)] = {"type": "INT", "value": None}
      cf2v2m += 1

    chri += 1

  # Return stats of numbers of conflicts generated
  return cf1v1m[0] + cf2v1m[0], cf1v1m[1] + cf2v1m[1], cf1v2m + cf2v2m
            
def wrap(s):
  """ Naive heuristic wrapping of string to try to improve dot display. """
  minwrap = 10
  if type(s) is str and len(s) > minwrap:
    n = int(sqrt(len(s)) * 2)
    s_ = '\\n'.join([s[ii:ii+n] for ii in range(0, len(s), n)])
    return s_
  else:
    return s

def rm_ctrl_chars(s):
  """ Attempt to remove control characters from string. """
  return control_char_re.sub('', s)

def genoid():
  """ IDs for CPN objects which are not directly mapped from BPMN objects. """
  oid = 0
  while True:
    yield oid
    oid += 1
